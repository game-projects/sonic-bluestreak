local Filesystem = {}

function Filesystem.exists(filepath)
  local info = love.filesystem.getInfo( filepath )
  local exists = false

  if (info == nil) then
    exists = false
  else
    exists = true
  end

  return exists
end

return Filesystem
