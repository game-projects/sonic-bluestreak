local cwd  = (...):gsub('%.init$', '') .. "."

return {
  math = require(cwd .. "math"),
  graphics = require(cwd .. "graphics"),
  filesystem = require(cwd .. "filesystem")
}
