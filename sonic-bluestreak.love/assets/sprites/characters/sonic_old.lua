return {
  metadata = {
    height      = 64,
    width       = 64,
    defaultAnim = "idle"
  },
  animations = {
    ["idle"] = {
      startAt     = 1,
      endAt       = 8,
      loop        = 1,
      speed       = 8,
      pauseAtEnd  = false,
    },
    ["crouch"] = {
      startAt     = 9,
      endAt       = 12,
      loop        = 12,
      speed       = 8,
      pauseAtEnd  = true,
    },
    ["walk"] = {
      startAt     = 13,
      endAt       = 20,
      loop        = 13,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["walk2"] = {
      startAt     = 21,
      endAt       = 28,
      loop        = 21,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["run"] = {
      startAt     = 29,
      endAt       = 36,
      loop        = 29,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["run2"] = {
      startAt     = 37,
      endAt       = 44,
      loop        = 37,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["dash"] = {
      startAt     = 45,
      endAt       = 52,
      loop        = 45,
      speed       = -1,
      pauseAtEnd  = false,
    },
    ["airup"] = {
      startAt     = 53,
      endAt       = 56,
      loop        = 54,
      speed       = 8,
      pauseAtEnd  = false,
    },
    ["fall"] = {
      startAt     = 57,
      endAt       = 61,
      loop        = 59,
      speed       = 8,
      pauseAtEnd  = false,
    },
    ["brake"] = {
      startAt     = 62,
      endAt       = 64,
      loop        = 62,
      speed       = 8,
      pauseAtEnd  = false,
    },
    ["jump"] = {
      startAt     = 70,
      endAt       = 75,
      loop        = 72,
      speed       = 16,
      pauseAtEnd  = false,
    },
    ["jumpaction"] = {
      startAt     = 72,
      endAt       = 75,
      loop        = 72,
      speed       = 16,
      pauseAtEnd  = false,
    },
    ["action"] = {
      startAt     = 76,
      endAt       = 79,
      loop        = 76,
      speed       = 16,
      pauseAtEnd  = false,
    },
    ["grind"] = {
      startAt     = 84,
      endAt       = 84,
      loop        = 84,
      speed       = 00,
      pauseAtEnd  = true,
    }
  }
}
