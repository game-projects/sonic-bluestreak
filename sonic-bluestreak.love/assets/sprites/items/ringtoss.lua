return {
  metadata = {
    height      = 24,
    width       = 32,
    defaultAnim = "default",
    oy = 12,
    ox = 20,
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 2,
      loop        = 1,
      speed       = 20,
      pauseAtEnd  = false,
    },
  }
}
