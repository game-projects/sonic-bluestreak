return {
  metadata = {
    height      = 16,
    width       = 16,
    defaultAnim = "default"
  },
  animations = {
    ["default"] = {
      startAt     = 1,
      endAt       = 8,
      loop        = 1,
      speed       = 8,
      pauseAtEnd  = false,
    },
  }
}
