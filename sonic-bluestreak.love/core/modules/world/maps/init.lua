local cwd = (...):gsub('%.init$', '') .. "."

local mapObjects = {}
mapObjects.Sti    = require(cwd .. "sti")
mapObjects.Base   = require(cwd .. "parent")

return mapObjects
