-- hitbox3D.lua :: a basic 3D hitbox object. It's used by the actors to check
-- collisions and to handle different type of responses.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Hitbox3D = Object:extend()

-- INIT FUNCTIONS
-- Initialise the actor and its base functions

function Hitbox3D:new(owner, type, ox, oy, oz, w, h, d, isSolid)
  self.owner    = owner
  self.world    = owner.world

  self.type     = type
  self.ox       = ox
  self.oy       = oy
  self.oz       = oz
  self.x, self.y, self.z = self:getPosition()
  self.w        = w
  self.h        = h
  self.d        = d
  self.isSolid  = isSolid

  self.isMainHitBox = false

  self:setDebugColor(0,0,0)
  self:register()
end

function Hitbox3D:advertiseAsMainHitbox()
  self.isMainHitBox = true
end

function Hitbox3D:modify(ox, oy, oz, w, h, d)
  self.ox       = ox
  self.oy       = oy
  self.oz       = oz
  self.x, self.y, self.z = self:getPosition()
  self.w        = w
  self.h        = h
  self.d        = d
end

function Hitbox3D:setDebugColor(r,g,b)
  self.debug = {}
  self.debug.r = r
  self.debug.g = g
  self.debug.b = b
end

function Hitbox3D:register()
  self.world:registerBody(self)
end

function Hitbox3D:destroy()
  self.world:removeBody(self)
end

-- COORDINATE FUNCTIONS
-- Handle Hitbox position

function Hitbox3D:updatePosition()
  self.x, self.y, self.z = self:getPosition()
  self.world:updateBody(self)
  return self.x, self.y, self.z
end

function Hitbox3D:getPosition()
  return self.ox + self.owner.x, self.oy + self.owner.y, self.oz + self.owner.z
end

function Hitbox3D:getOwnerPosition()
  return self.x - self.ox, self.y - self.oy, self.z - self.oz
end

function Hitbox3D:getNewOwnerPosition(x, y, z)
  return x - self.ox, y - self.oy, z - self.oz
end

function Hitbox3D:getCenter()
  return self.x + (self.w/2), self.y + (self.h/2), self.z + (self.d/2)
end

-- COLLISION FUNCTIONS
-- Handle Hitbox position

function Hitbox3D:checkCollision(dx, dy, dz, filter)
  self:updatePosition()

  local dx, dy = self.ox + dx, self.oy + dy, self.oz + dz
  local x, y, z, cols, colNumber = self.world:checkCollision(self, dx, dy, dz, filter)
  local newx, newy, newz = self:getNewOwnerPosition(x, y, z)

  return newx, newy, newz, cols, colNumber
end

-- DRAW FUNCTIONS
-- Just some debug function to draw hitbox

function Hitbox3D:draw()
  local x, y, z = self:getPosition()
  love.graphics.setColor(self.debug.r, self.debug.g, self.debug.b, 1)
  utils.graphics.box(x, (y-z) - (self.d), self.w, self.h)
  love.graphics.setColor(self.debug.r/2, self.debug.g/2, self.debug.b/2, 1)
  utils.graphics.box(x, (y-z) - (self.d) + (self.h), self.w, self.d)
  utils.graphics.resetColor()
end

return Hitbox3D
