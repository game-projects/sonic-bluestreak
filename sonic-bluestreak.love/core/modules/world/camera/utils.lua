-- camutils.lua :: some camera utilities

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local camutils = {}

function camutils.getViewsPositions(basewidth, baseheight, verticalSplit)
  local posList = {}

  posList.dual  = {}
  posList.multi = {}

  if (verticalSplit) then
    posList.dual[1] = {}
    posList.dual[1].x = 0
    posList.dual[1].y = (baseheight/4)

    posList.dual[2] = {}
    posList.dual[2].x = 0
    posList.dual[2].y = -(baseheight/4)
  else
    posList.dual[1] = {}
    posList.dual[1].x = -(basewidth/4)
    posList.dual[1].y = 0

    posList.dual[2] = {}
    posList.dual[2].x = (basewidth/4)
    posList.dual[2].y = 0
  end

  posList.multi[1] = {}
  posList.multi[1].x = -(basewidth /4)
  posList.multi[1].y = -(baseheight/4)

  posList.multi[2] = {}
  posList.multi[2].x =  (basewidth /4)
  posList.multi[2].y = -(baseheight/4)

  posList.multi[3] = {}
  posList.multi[3].x = -(basewidth /4)
  posList.multi[3].y =  (baseheight/4)

  posList.multi[4] = {}
  posList.multi[4].x =  (basewidth /4)
  posList.multi[4].y =  (baseheight/4)

  return posList
end

function camutils.getViewsDimensions(viewnumber, basewidth, baseheight, verticalSplit)
  if (viewnumber <= 1) then
    return basewidth,       baseheight
  elseif (viewnumber == 2) then
    if (verticalSplit) then
      return (basewidth),     (baseheight/2)
    else
      return (basewidth/2),   (baseheight)
    end
  else
    return (basewidth/2),   (baseheight/2)
  end
end

return camutils
