-- camera.lua :: a basic camera adapted to the asset/world system.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd   = (...):gsub('%.init$', '') .. "."

local CameraSystem  = Object:extend()
--local View          = require(cwd .. "libs.hump.camera")

local camutils      = require(cwd .. "utils")

local SPLITSCREEN_ISVERTICAL  = false
local SCREEN_LIMIT            = 4

-- INIT FUNCTIONS
-- Initialize the camera system

function CameraSystem:new(world)
  self.scene = world.scene
  self.world = world

  self.mode           = "split"
  self.verticalSplit  = SPLITSCREEN_ISVERTICAL
  self.targets = {}

  self.xLocked = nil
  self.yLocked = nil

  self:initViews()
end

function CameraSystem:setMode(mode)
  self.mode = mode
  core.debug:print("camera", "mode is now set to " .. mode)
  return mode
end

function CameraSystem:initViews()
  self.views = {}

  self.views.list   = {}
  self.views.basewidth, self.views.baseheight   = core.screen:getDimensions()
  self.views.width,     self.views.height       = self:getViewsDimensions()

  self.views.posList = camutils.getViewsPositions(self.views.basewidth, self.views.baseheight, self.verticalSplit)
end

-- INFO FUNCTIONS
-- Get informations from the camera system

function CameraSystem:getViewNumber()
  return #self.views.list
end

function CameraSystem:haveView()
  return (self:getViewNumber() == 0)
end

function CameraSystem:getViewsDimensions()
  local basewidth, baseheight = self.views.basewidth, self.views.baseheight
  local viewnumber = self:getViewNumber()

  return camutils.getViewsDimensions(viewnumber, basewidth, baseheight, self.verticalSplit)
end

function CameraSystem:recalculateViewsPositions()
  if #self.views.list == 1 then
    self.views.list[1].onScreen.x = 0
    self.views.list[1].onScreen.y = 0
  else
    for i,v in ipairs(self.views.list) do
      local x, y = self:getViewPositions(i)
      self.views.list[i].onScreen.x = x
      self.views.list[i].onScreen.y = y
    end
  end
end

function CameraSystem:getViewPositions(id)
  local viewNumber = #self.views.list

  if (viewNumber == 2) and ((id == 1) or (id == 2)) then
    return self.views.posList.dual[id].x,   self.views.posList.dual[id].y
  elseif (viewNumber > 2) then
    return self.views.posList.multi[id].x,  self.views.posList.multi[id].y
  end

end

function CameraSystem:lockX(x)
  self.xLocked = x
end

function CameraSystem:unlockX()
  self.xLocked = nil
end

function CameraSystem:lockY(y)
  self.yLocked = y
end

function CameraSystem:unlockY()
  self.yLocked = nil
end


-- WRAPPER and UTILS
-- Access data from the views

function CameraSystem:addTarget(target)
  if (#self.views < SCREEN_LIMIT) then
    if (#self.views.list == 0) or (self.mode == "split") then
      local x, y = target:getViewCenter()
      self:addView(x, y, target)
      table.insert(self.targets, target)
    elseif (self.mode == "middle") or (self.mode == "zoom") then
      table.insert(self.targets, target)
    end
  end
end

function CameraSystem:addView(x, y, target)
  if (#self.views.list < SCREEN_LIMIT) then
    local id = #self.views.list + 1
    local view = {}

    view.pos = {}
    view.x = x or 0
    view.y = y or 0
    view.scale  = 1

    view.onScreen = {}

    -- TODO: add a target system in order to make a camera able
    -- to target a specific object
    view.target = target

    table.insert(self.views.list, view)
    self.views.width, self.views.height = self:getViewsDimensions()
    self:regenerateCanvases()
    self:recalculateViewsPositions()
  end
end

function CameraSystem:regenerateCanvases()
  for i, view in ipairs(self.views.list) do
    view.canvas = love.graphics.newCanvas(self.views.width, self.views.height)
  end
end

function CameraSystem:getView(id)
  return self.views.list[id]
end

function CameraSystem:attachView(id)
  if (id ~= nil) then
    local view = self:getView(id)

    self.current_canvas = love.graphics.getCanvas()
  	love.graphics.setCanvas(view.canvas)
  	love.graphics.clear()

    if id ~= nil then
    -- Du à la manière dont fonctionne STI, on est obligé de récupérer les info
    -- de position de camera pour afficher la carte par rapport à ces infos
      tx, ty  = self:getViewCoordinate(id)
      scale   = self:getViewScale(id) or 1
    	tx = math.floor(tx) * -1
    	ty = math.floor(ty) * -1

      local w, h = core.screen:getDimensions()
    end

    love.graphics.push()
  	love.graphics.origin()
  	love.graphics.translate(math.floor(tx), math.floor(ty))
  end
end

function CameraSystem:detachView(id)
  if (id ~= nil) then
    local view = self:getView(id)
    love.graphics.pop()

    love.graphics.setCanvas(self.current_canvas)
    local tx, ty  = self:getOnScreenViewCoordinate(id)
    local scale   = core.screen:getScale() * view.scale

    love.graphics.push()
  	love.graphics.origin()
  	love.graphics.translate(math.floor(tx), math.floor(ty))
    love.graphics.scale(scale, scale)

    love.graphics.draw(view.canvas)

    local unscale = 1 / view.scale
    love.graphics.scale(unscale, unscale)
    self:drawHUD(id)

    love.graphics.pop()
  end
end

function CameraSystem:getViewCoordinate(id)
  local view  = self:getView(id)

  local viewx, viewy, vieww, viewh

  vieww = self.views.width  / view.scale
  viewh = self.views.height / view.scale

  viewx = view.x - (vieww / 2)
  viewy = view.y - (viewh / 2)

  return viewx, viewy, vieww, viewh
end

function CameraSystem:getOnScreenViewCoordinate(id)
  local view = self:getView(id)

  local viewx, viewy, vieww, viewh
  local basex, basey = (self.views.basewidth  / 2), (self.views.baseheight / 2)
  viewx   = (basex) + view.onScreen.x - (self.views.width   / 2)
  viewy   = (basey) + view.onScreen.y - (self.views.height  / 2)

  viewx, viewy = core.screen:getScreenCoordinate(viewx, viewy)

  vieww   = self.views.width  * core.screen:getScale()
  viewh   = self.views.height * core.screen:getScale()
  return viewx, viewy, vieww, viewh
end

function CameraSystem:getOnScreenViewRelativePosition(id)
  local view = self:getView(id)

  local viewx, viewy
  local basex, basey = (self.views.basewidth  / 2), (self.views.baseheight / 2)
  viewx   = view.onScreen.x
  viewy   = view.onScreen.y

  return core.screen:getScreenCoordinate(viewx, viewy)
end

function CameraSystem:getOnScreenViewCenter(id)
  local view = self:getView(id)

  local viewx, viewy
  local basex, basey = (self.views.basewidth  / 2), (self.views.baseheight / 2)
  viewx   = (basex) + view.onScreen.x
  viewy   = (basey) + view.onScreen.y

  return core.screen:getScreenCoordinate(viewx, viewy)
end

function CameraSystem:setViewScale(id, scale)
  local view = self:getView(id)

  view.scale = scale

  local _, _, w, h = self:getViewCoordinate(id)

  view.canvas = love.graphics.newCanvas(math.ceil(w), math.ceil(h))
end

function CameraSystem:getViewScale(id)
  local view = self:getView(id)

  return view.scale
end

function CameraSystem:limitView(id)
  local viewx, viewy, vieww, viewh      = self:getViewCoordinate(id)
  local worldx, worldy, worldw, worldh = self.world:getBox()
  local posx = self.views.list[id].x
  local posy = self.views.list[id].y
  local minx = worldx + self.views.width  / 2
  local miny = worldy + self.views.height / 2
  local maxx = worldw - self.views.width  / 2
  local maxy = worldh - self.views.height / 2

  self.views.list[id].x = utils.math.between(posx, minx, maxx)
  self.views.list[id].y = utils.math.between(posy, miny, maxy)

  if (self.xLocked ~= nil) then
    self.views.list[id].x = self.xLocked
  end

  if (self.yLocked ~= nil) then
    self.views.list[id].y = self.yLocked
  end
end

-- UPDATE and MOVE functions
-- Move and update the camera system

function CameraSystem:update(dt)
  for i,v in ipairs(self.views.list) do
    if (self.mode == "middle") or (self.mode == "zoom") then
      self:followAllActors(i)
    else
      self:followActor(i)
    end
  end
end

function CameraSystem:moveView(id, x, y)
  self.views.list[id].x = x
  self.views.list[id].y = y

  self:limitView(id)
end

function CameraSystem:followActor(id)
  local view = self:getView(id)

  if view.target ~= nil then
    local x, y = view.target:getViewCenter()
    x = math.floor(x)
    y = math.floor(y)
    self:moveView(id, x, y)
  end
end

function CameraSystem:followAllActors(id)
  local view = self:getView(id)
  local PADDING = 16
  -- TODO: make all the padding and stuff part of object definition instead ?
  -- It would especially allow better work in future fake3D worlds

  if (#self.targets > 0) then
    local minX, minY, maxX, maxY
    for i, target in ipairs(self.targets) do
      local x, y, w, h = target:getShape()
      local x2, y2 = x + w, y + h
      -- If start by initializing the value at the first found value
      if (minX == nil) then minX = x  end
      if (maxX == nil) then maxX = x2 end
      if (minY == nil) then minY = y  end
      if (maxY == nil) then maxY = y2 end


      minX = math.min(minX, x)
      maxX = math.max(maxX, x2)
      minY = math.min(minY, y)
      maxY = math.max(maxY, y2)
    end

    -- Add padding
    minX = minX - PADDING
    minY = minY - PADDING
    maxX = maxX + PADDING
    maxY = maxY + PADDING
    local x, y
    x = (minX + maxX) / 2
    y = (minY + maxY) / 2

    local scale = 1
    if (self.mode == "zoom") then
      local ww, hh = core.screen:getDimensions()
      local scalex = (maxX-minX)/ww
      local scaley = (maxY-minY)/hh
      scale = math.max(scale, scalex, scaley)
      self:setViewScale(id, 1/scale)
      self.world:resizeMap(ww * 3, hh * 3)
    end

    self:moveView(id, x, y)
  end
end


-- DRAW FUNCTIONS
-- Basic callback to draw stuff

function CameraSystem:drawHUD(id)
  local view = self:getView(id)
  local viewx, viewy, vieww, viewh = self:getOnScreenViewCoordinate(id)

  view.target:drawHUD(id, vieww, viewh)
end

return CameraSystem
