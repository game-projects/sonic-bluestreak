-- world3D.lua :: a basic fake3D world based on bump-2dpd.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd   = (...):gsub('%.world3D$', '') .. "."

local BaseWorld = require(cwd .. "baseworld")
local World3D   = BaseWorld:extend()

local Bump          = require(cwd .. "libs.bump")
local Bump3D        = require(cwd .. "libs.bump-3dpd")
local Tsort         = require(cwd .. "libs.tsort")
local CameraSystem  = require(cwd .. "camera")

local PADDING_VALUE = 10/100

function World3D:new(scene, actorlist, mapfile, maptype)
  World3D.super.new(self, scene, actorlist, mapfile, maptype)
end

-- ACTORS FUNCTIONS
-- Add support for bodies in Actor functions

function World3D:initActors()
  self.currentCreationID  = 0
  self.actors = {}
  self.bodies = Bump3D.newWorld(50)
  self:initShapes()
  self:initTerrain()
end

function World3D:newActor(name, x, y, z)
  self.obj.index[name](self, x, y, z)
end

function World3D:newCollision(name, x, y, z, w, h, d)
  self.obj.collisions[name](self, x, y, z, w, h, d)
end

function World3D:moveActor(actor, x, y, z, filter)
  return self.bodies:move(actor.mainHitbox, x, y, z, filter)
end

function World3D:getActorsInRect(x, y, w, h)
  return self:getShapeInRect(x, y, w, h)
end

function World3D:getVisibleActors(id)
  local actors = {}
  if (id ~= nil) then
    local camx, camy, camw, camh = self.cameras:getViewCoordinate(id)
    local paddingw  = camw * PADDING_VALUE
    local paddingh  = camh * PADDING_VALUE
    local x = camx - paddingw
    local y = camy - paddingh
    local w = camw + paddingw * 2
    local h = camh + paddingh * 2

    actors = self:getActorsInRect(x, y, w, h)
  else
    actors = self:getActors()
  end

  actors = self:zSortItems(actors)

  return actors
end


-- PLAYER FUNCTIONS
-- Load player stuff

function World3D:newPlayer(id, x, y, z)
  return self.obj.Player(self, x, y, z, id)
end

-- BODIES MANAGEMENT FUNCTIONS
-- Basic function to handle bodies. Wrappers around Bump2D functions

function World3D:registerBody(body)
  return self.bodies:add(body, body.x, body.y, body.z, body.w, body.h, body.d)
end

function World3D:updateBody(body)
  return self.bodies:update(body, body.x, body.y, body.z, body.w, body.h, body.d)
end

function World3D:removeBody(body)
  return self.bodies:remove(body)
end

function World3D:checkCollision(body, x, y, z, filter)
  return self.bodies:check(body, x, y, z, filter)
end

function World3D:getBodiesInRect(x, y, w, h)
  return {} --self.bodies:queryRect(x, y, w, h)
end

-- UPDATE
-- Update everything in the world

function World3D:updateActors(dt)
  World3D.super.updateActors(self, dt)
  local actors = self:getActors()
  for i,v in ipairs(actors) do
    v:redrawShadowCanvas()
  end
end

-- SHAPE SYSTEM
-- Handle onscreen shapes

function World3D:initShapes()
  self.shapes = Bump.newWorld(50)
end

function World3D:registerShape(actor)
  local x, y, w, h = actor:getShape()
  return self.shapes:add(actor, x, y, w, h)
end

function World3D:updateShape(actor)
  local x, y, w, h = actor:getShape()
  return self.shapes:update(actor, x, y, w, h)
end

function World3D:removeShape(actor)
  return self.shapes:remove(actor)
end

function World3D:checkShapeIntersection(actor, x, y)
  return self.shapes:check(actor, x, y)
end

function World3D:getShapeInRect(x, y, w, h)
  return self.shapes:queryRect(x, y, w, h)
end

-- TERRAIN SYSTEM
-- Handle onscreen shapes

function World3D:initTerrain()
  self.terrains = Bump.newWorld(50)
end

function World3D:registerTerrain(actor)
  return self.terrains:add(actor, actor.x, actor.y, actor.w, actor.h)
end

function World3D:updateTerrain(actor)
  return self.terrains:update(actor, actor.x, actor.y, actor.w, actor.h)
end

function World3D:removeTerrain(actor)
  return self.terrains:remove(actor)
end

function World3D:getTerrainInRect(x, y, w, h)
  return self.terrains:queryRect(x, y, w, h)
end

-- DRAW FUNCTIONS
-- Functions to draw the world

function World3D:zSortItems(items)
  -- zSorting algorithm taken from bump3D example, adapted to gamecore.
  local graph = Tsort.new()
  local noOverlap = {}

  -- Iterate through all visible items, and calculate ordering of all pairs
  -- of overlapping items.
  -- TODO: Each pair is calculated twice currently. Maybe this is slow?
  for _, itemA in ipairs(items) do repeat
    local x, y, w, h = self.shapes:getRect(itemA)
    local otherItemsFilter = function(other) return other ~= itemA end
    local overlapping, len = self.shapes:queryRect(x, y, w, h, otherItemsFilter)

    if len == 0 then
      table.insert(noOverlap, itemA)

      break
    end

    local _, aY, aZ, _, aH, aD = self.bodies:getCube(itemA.mainHitbox)
    aDepth  = itemA.depth
    aID     = itemA.creationID
    aType   = itemA.type
    aZ     = math.ceil(aZ)
    aY     = math.ceil(aY)

    for _, itemB in ipairs(overlapping) do
      local _, bY, bZ, _, bH, bD = self.bodies:getCube(itemB.mainHitbox)
      bDepth = itemB.depth
      bID    = itemB.creationID
      bType  = itemB.type
      bZ     = math.ceil(bZ)
      bY     = math.ceil(bY)

      if aZ >= bZ + bD then
        -- item A is completely above item B
        graph:add(itemB, itemA)
      elseif bZ >= aZ + aD then
        -- item B is completely above item A
        graph:add(itemA, itemB)
      elseif aY + aH <= bY then
        -- item A is completely behind item B
        graph:add(itemA, itemB)
      elseif bY + bH <= aY then
        -- item B is completely behind item A
        graph:add(itemB, itemA)
      elseif (aY - aZ) + aH > (bY - bZ) + bH then
        -- item A's forward-most point is in front of item B's forward-most point
        graph:add(itemB, itemA)
      elseif (aY - aZ) + aH < (bY - bZ) + bH then
        -- item B's forward-most point is in front of item A's forward-most point
        graph:add(itemA, itemB)
      else
        -- item A's forward-most point is the same than item B's forward-most point
        if aDepth > bDepth then
          graph:add(itemB, itemA)
        elseif aDepth < bDepth then
          graph:add(itemA, itemB)
        else
          if aID > bID then
            graph:add(itemA, itemB)
          elseif aID < bID then
            graph:add(itemB, itemA)
          else
            err("two object can't have the same ID")
          end
        end
      end
    end
  until true end

  local sorted, err = graph:sort()
  if err then
    error(err)
  end
  for _, item in ipairs(noOverlap) do
    table.insert(sorted, item)
  end

  return sorted

end

return World3D
