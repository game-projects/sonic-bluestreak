-- parent.lua : The parent of the functions.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Menu = Object:extend()

local function updateWidgetByOrder(a, b)
  if a.order ~= b.order then
    return a.order < b.order
  else
    return a.creationID < b.creationID
  end
end

-- INIT FUNCTIONS
-- Initialize and configure functions.

function Menu:new(menusystem, name, x, y, w, h)
  self.menusystem = menusystem
  self.name       = name

  self.x = x
  self.y = y
  self.w = w
  self.h = h

  self.widget = {}
  self.widget.list              = {}
  self.widget.selected          = 0
  self.widget.selectedPrevious  = 0
  self.widget.cancel            = 0
  self:updateWidgetSize()

  self.isDestroyed      = false
  self.isVisible        = true
  self.isActive         = true
  self.isLocked         = false
  self.isAlwaysVisible  = false

  self.depth        = 0

  self:resetSound()

  self:register()
end

function Menu:setDepth(depth)
  self.depth = depth or 0
end

function Menu:setVisibility(visibility)
  if self.isLocked == false and self.isAlwaysVisible == false then
    -- if the menu is locked (thus is always active), it should also
    -- be always visible.
    self.isVisible = visibility
  else
    self.isVisible = true
  end
end

function Menu:setActivity(activity)
  if self.isLocked == false then
    self.isActive = activity
  else
    self.isActive = true
  end
end

function Menu:lock(lock)
  self.isLocked = lock
end

function Menu:lockVisibility(lock)
  self.isAlwaysVisible = lock
end

function Menu:getFocus()
  self.menusystem.focusedMenu = self.name
end

function Menu:haveFocus()
  return (self.menusystem.focusedMenu == self.name)
end

function Menu:register()
  self.menusystem:addMenu(self.name, self)
end

function Menu:setCancelWidget(id)
  self.widget.cancel = #self.widget.list
end

function Menu:updateWidgetSize()
  self.widget.h = 0
  self.widget.w = 0
end

function Menu:getWidgetSize(id)
  return self.widget.w, self.widget.h
end

function Menu:getWidgetNumber()
  return #self.widget.list
end

-- ACTION FUNCTIONS
-- Send actions to the widgets

function Menu:cancelAction()
  if (self.widget.cancel ~= 0) then
    self.widget.list[self.widget.cancel]:action("key")
  end
end

function Menu:clear()
  self.widget.list = {}
  self.widget.cancel = 0
end

function Menu:resize(x,y,w,h)
  self.x = x
  self.y = y
  self.w = w
  self.h = h

  self:updateWidgetSize()
end

function Menu:destroy()
  self.destroyed = true
end

function Menu:updateWidgetsOrder()
  table.sort(self.widget.list, updateWidgetByOrder)
end

-- UPDATE FUNCTIONS
-- Update the menu every game update

function Menu:update(dt)
  -- Cette fonction ne contient rien par défaut
  self:updateSelectedWidget(dt)
end

function Menu:updateSelectedWidget(dt)
  if (self.widget.selected ~= self.widget.previous) and (self.isActive) then
    if (self.widget.list[self.widget.selected] ~= nil) then
      self.widget.list[self.widget.selected]:selectAction()
      self.widget.previous = self.widget.selected
    end
  end
end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function Menu:draw()
  -- nothing here
end

function Menu:drawCursor()
  -- nothing here
end

function Menu:drawCanvas()

end

-- KEYBOARD FUNCTIONS
-- Handle key press

function Menu:keyreleased(key)
  -- Cette fonction ne contient rien par défaut
end

-- MOUSE FUNCTIONS
-- Handle pointers (clic/touch)

function Menu:mousemoved(x, y)
  -- Cette fonction ne contient rien par défaut
end

function Menu:mousepressed( x, y, button, istouch )
  -- Cette fonction ne contient rien par défaut
end

-- WIDGET FUNCTIONS
-- Handle widgets of the functions

function Menu:addWidget(newwidget)
  if #self.widget.list == 0 then
    self.widget.selected = 1
  end
  table.insert(self.widget.list, newwidget)
  self:updateWidgetsID()
  self:updateWidgetsOrder()
end

function Menu:updateWidgets(dt)
  self:removeDestroyedWidgets()
  for i,v in ipairs(self.widget.list) do
    v.id = i
    v:update(dt)
  end
end

function Menu:updateWidgetsID()
  for i,v in ipairs(self.widget.list) do
    v.id = i
  end
end

function Menu:removeDestroyedWidgets() -- On retire les widgets marquées comme supprimées
  for i,v in ipairs(self.widget.list) do
    if (v.destroyed == true) then
      table.remove(self.widget.list, i)
    end
  end
end

-- CURSOR FUNCTIONS
-- Set or move the cursor of the menu

function Menu:setCursor(cursorid)
  self.widget.selected = cursorid --math.max(1, math.min(cursorid, #self.widget.list))
end

function Menu:moveCursor(new_selected)
  self:playNavigationSound()
  if new_selected < 1 then
    self.widget.selected = #self.widget.list + new_selected
  else
    if new_selected > #self.widget.list then
      self.widget.selected = new_selected - #self.widget.list
    else
      self.widget.selected = new_selected
    end
  end
end

-- SOUND FUNCTION
-- Handle SFX

function Menu:resetSound()
  self.sound = {}
  self.sound.active = false
  self.sound.asset  = nil
end

function Menu:setSoundFromSceneAssets(name)
  self:setSound(self.menusystem.scene.assets.sfx[name])
end

function Menu:setSound(soundasset)
  self.sound.active = true
  self.sound.asset = soundasset
end

function Menu:playNavigationSound()
  if self.sound.active == true then
    love.audio.stop( self.sound.asset )
    self.sound.asset:setVolume(core.options.data.audio.sfx / 100)
    love.audio.play( self.sound.asset )
  end
end

-- VIEW FUNCTIONS
-- Handle the view of the menu

function Menu:resetView()
  -- ne sert à rien ici, c'est juste pour éviter des crash
end

function Menu:updateView()
  -- ne sert à rien ici, c'est juste pour éviter des crash
end

function Menu:moveView()
  -- ne sert à rien ici, c'est juste pour éviter des crash
end

return Menu
