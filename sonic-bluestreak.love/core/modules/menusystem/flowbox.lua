-- flowbox :: flexible box menu, that handle in grid the widgets

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.flowbox$', '') .. "."

local Menu = require(cwd .. "parent")
local FlowBox = Menu:extend()

local menuutils = require(cwd .. "widgets.utils")

-- INIT FUNCTIONS
-- Initialize and configure the flowbox

function FlowBox:new(menusystem, name, x, y, w, h, slots_hor, slots_vert)
  self.view = {}
  self.view.slotNumber  = slots_hor * slots_vert
  self.view.lineNumber  = slots_vert
  self.view.colNumber   = slots_hor
  self.view.firstSlot   = 1
  FlowBox.super.new(self, menusystem, name, x, y, w, h)
  self.widget.h = math.floor( self.h / slots_vert )
  self.widget.w = math.floor( self.w / slots_hor )
  self.h = slots_vert * self.widget.h -- On fait en sorte que la hauteur
  self.w = slots_hor * self.widget.w -- et la largeur
  -- soit un multiple du nombre de slot et de leur dimensions
end

-- UPDATE FUNCTIONS
-- Update the menu and its view

function FlowBox:updateWidgetSize()
  self.widget.h = math.floor( self.h / self.view.lineNumber )
  self.widget.w = math.floor( self.w / self.view.colNumber )
end

function FlowBox:update(dt)
  self:updateView()
  self:updateSelectedWidget(dt)
end

function FlowBox:updateView()
  local col, line = self:getCoord(self.widget.selected)
  local begincol, beginline = self:getCoord(self.view.firstSlot)

  if line < beginline then
    beginline = line
  end

  if line > beginline + self.view.lineNumber - 1 then
    beginline = line - self.view.lineNumber + 1
  end

  if beginline < 0 then
    beginline = 0
  end

  self.view.firstSlot = beginline * self.view.colNumber + 1
end

-- INFO FUNCTIONS
-- Get informations

function FlowBox:getCoord(id_selected)
  id_selected = id_selected - 1 -- On simplifie les calcul en prenant 0 comme départ
  local line, col
  line = math.floor(id_selected / self.view.colNumber)
  col = id_selected - (line * self.view.colNumber)
  return col, line
end

-- CURSOR FUNCTIONS
-- Handle the cursor in a 2D menu

function FlowBox:moveCursor(new_col, new_line)
  local col, line = self:getCoord(self.widget.selected)
  local lastcol, lastline = self:getCoord(#self.widget.list)


  if new_line < 0 then
    new_line = lastline
  end

  if new_line > lastline then
    new_line = 0
  end

  if (new_line == lastline) then
    if new_col < 0 then
      new_col = lastcol
    end

    if new_col > lastcol then
      if (line == lastline) then
      new_col = 0
      else
      new_col = lastcol
      end
    end
  else
    if new_col < 0 then
      new_col = self.view.colNumber - 1
    end

    if new_col == self.view.colNumber then
      new_col = 0
    end
  end

  self.widget.selected = (new_line * self.view.colNumber) + new_col + 1
end

-- KEYS FUNCTIONS
-- Handle the keyboard/controller inputs

function FlowBox:keyreleased(key, code)
  local col, line = self:getCoord(self.widget.selected)
  if key == 'left' then
    self:moveCursor(col - 1, line)
  end

  if key == 'right' then
    self:moveCursor(col + 1, line)
  end

  if key == 'up' then
    self:moveCursor(col, line - 1)
  end

  if key == 'down' then
    self:moveCursor(col, line + 1)
  end

  if key == "A" then
    if (self.widget.selected >= 1 and self.widget.selected <= #self.widget.list) then
      self.widget.list[self.widget.selected]:action("key")
    end
  end
end

-- MOUSE FUNCTIONS
-- Handle the mouse/touch pointer

function FlowBox:mousemoved(x, y)
  local col, line = self:getCoord(self.widget.selected)
  local begincol, beginline = self:getCoord(self.view.firstSlot)
  local newcol, newline, widget_selected

  newline = beginline + math.floor(y / self.widget.h)
  newcol  = math.floor(x / self.widget.w)
  widget_selected = (newline * self.view.colNumber) + newcol + 1

  if widget_selected >= 1 and widget_selected <= #self.widget.list then
    self.widget.selected = widget_selected
    self:getFocus()
  end
end

function FlowBox:mousepressed(x, y, button, isTouch)
  local col, line = self:getCoord(self.widget.selected)
  local begincol, beginline = self:getCoord(self.view.firstSlot)
  local newline, newcol, widget_selected

  newline = beginline + math.floor(y / self.widget.h)
  newcol  = math.floor(x / self.widget.w)
  widget_selected = (newline * self.view.colNumber) + newcol + 1

  if widget_selected >= 1 and widget_selected <= #self.widget.list then
    self.widget.selected = widget_selected
    self:getFocus()
    self.widget.list[self.widget.selected]:action("pointer")
  end

end

-- DRAW FUNCTIONS
-- Draw the menu and its content

function FlowBox:draw()
  self:updateView()
  local widgety = self.y
  local widgetx = self.x
  for i,v in ipairs(self.widget.list) do
    if (i >= self.view.firstSlot) and (i < self.view.firstSlot + self.view.slotNumber) then
      v:draw(widgetx, widgety, self.widget.w, self.widget.h)
      if self.widget.selected == i and self:haveFocus() == true then
        v:drawSelected(widgetx, widgety, self.widget.w, self.widget.h)
      else
        v:draw(widgetx, widgety, self.widget.w, self.widget.h)
      end
      widgetx = widgetx + self.widget.w
      if widgetx == (self.x + self.w) then
        widgetx = self.x
        widgety = widgety + self.widget.h
      end
    end
  end
end

function FlowBox:drawCursor()
  self:updateView()
  local begincol, beginline = self:getCoord(self.view.firstSlot)
  if (self.widget.selected >= 1 and self.widget.selected <= #self.widget.list) then
    local w, h  = self:getWidgetSize()
    local col, line = self:getCoord(self.widget.selected)
    local x     = (col) * h
    local y     = (line - beginline) * h
    menuutils.drawCursor(self.x + x, self.y + y, w, h)
  end
end

return FlowBox
