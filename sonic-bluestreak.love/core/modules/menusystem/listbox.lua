-- listbox : add a vertical list of widgets.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.listbox$', '') .. "."
local Menu = require(cwd .. "parent")

local ListBox = Menu:extend()

local menuutils = require(cwd .. "widgets.utils")

-- INIT FUNCTIONS
-- Initialize and configure functions.

function ListBox:new(menusystem, name, x, y, w, h, slotNumber)
  self.view = {}
  self.view.slotNumber  = slotNumber
  self.view.firstSlot   = 1
  ListBox.super.new(self, menusystem, name, x, y, w, h)
  self.h = slotNumber * self.widget.h -- On fait en sorte que la hauteur
  -- soit un multiple du nombre de slot et de leur hauteur
end

-- UPDATE FUNCTIONS
-- Update the menu every step.

function ListBox:updateWidgetSize()
  self.widget.h = math.floor( self.h / self.view.slotNumber )
  self.widget.w = self.w
end

function ListBox:update(dt)
  self:updateView()
  self:updateSelectedWidget(dt)
end

function ListBox:updateView()
  if self.widget.selected < self.view.firstSlot then
    self.view.firstSlot = self.widget.selected
  end
  if self.widget.selected > self.view.firstSlot + self.view.slotNumber - 1 then
    self.view.firstSlot = self.widget.selected - self.view.slotNumber + 1
  end

  if self.view.firstSlot < 1 then
    self.view.firstSlot = 1
  end
end

-- KEYBOARD FUNCTIONS
-- Handle input from keyboard/controllers.

function ListBox:keyreleased(key, code)

  if key == 'up' then
    self:moveCursor(self.widget.selected - 1)
  end

  if key == 'down' then
    self:moveCursor(self.widget.selected + 1)
  end

  if key == "A" then
    if (self.widget.selected >= 1 and self.widget.selected <= #self.widget.list) then
      self.widget.list[self.widget.selected]:action("key")
    end
  end

  if key == "B" then
    if (self.widget.cancel >= 1 and self.widget.cancel <= #self.widget.list) then
      self.widget.list[self.widget.cancel]:action("key")
    end
  end

end

-- MOUSE FUNCTIONS
-- Handle input from pointers.

function ListBox:mousemoved(x, y)
  local widget_selected = self.view.firstSlot + math.floor(y / self.widget.h)

  if widget_selected >= 1 and widget_selected <= #self.widget.list then
    self.widget.selected = widget_selected
    self:getFocus()
  end
end

function ListBox:mousepressed(x, y, button, isTouch)
  local widget_selected = self.view.firstSlot + math.floor(y / self.widget.h)

  if widget_selected >= 1 and widget_selected <= #self.widget.list then
    self.widget.selected = widget_selected
    self:getFocus()
    if #self.widget.list > 0 then
      self.widget.list[self.widget.selected]:action("pointer")
    end
  end

end

-- DRAW FUNCTIONS
-- draw the menu and the rest of content.

function ListBox:draw()
  self:updateView()
  local widgety = self.y
  for i,v in ipairs(self.widget.list) do
    if (i >= self.view.firstSlot) and (i < self.view.firstSlot + self.view.slotNumber) then
      v:draw(self.x, widgety, self.w, self.widget.h)
      if self.widget.selected == i and self:haveFocus() == true then
        v:drawSelected(self.x, widgety, self.w, self.widget.h)
      else
        v:draw(self.x, widgety, self.w, self.widget.h)
      end
      widgety = widgety + self.widget.h
    end
  end
end

function ListBox:drawCursor()
  self:updateView()
  if (self.widget.selected >= 1 and self.widget.selected <= #self.widget.list) then
    local w, h  = self:getWidgetSize()
    local y     = (self.widget.selected - self.view.firstSlot) * h
    menuutils.drawCursor(self.x,self.y + y, w, h)
  end
end

return ListBox
