-- assets/sprite :: the background object, which is an image that draw itself
-- automatically to fill a texture.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Background = Object:extend()

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Background:new(filepath)
  self.image    = love.graphics.newImage(filepath)
  self.batch    = love.graphics.newSpriteBatch(self.image , 1000 )

  self.width, self.height   = self.image:getDimensions()
  screenwidth, screenheight = core.screen:getDimensions()

  local w = math.floor(screenwidth / self.width)    * self.width   + 1
  local h = math.floor(screenheight / self.height)  * self.height  + 1

  for i=-1, w do
    for j=-1, h do
      self.batch:add(i * self.width, j * self.height)
      j = j + 1
    end
    i = i + 1
  end
end

function Background:draw(ox, oy)
  love.graphics.setColor(1, 1, 1)
  love.graphics.draw(self.batch, ox, oy)
end

return Background
