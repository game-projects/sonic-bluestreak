-- assets/texture :: the texture object, essentially used to be able to draw easily
-- the mask of the texture (used for stuff like flashing sprite, etc)

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]


local Texture = Object:extend()

local function getMask(x, y, r, g, b, a)
    -- template for defining your own pixel mapping function
    -- perform computations giving the new values for r, g, b and a
    -- ...
    return 1, 1, 1, a
end

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Texture:new(filename)
  self.imageData = love.image.newImageData(filename)

  local maskData  = self.imageData:clone()
  maskData:mapPixel( getMask )

  self.image = love.graphics.newImage( self.imageData )
  self.mask  = love.graphics.newImage( maskData )
end

-- INFO FUNCTIONS
-- get information with these functions

function Texture:getDimensions()
  return self.image:getDimensions()
end

-- DRAW FUNCTIONS
-- Draw texture using these functions

function Texture:draw(x, y, r, sx, sy, ox, oy, kx, ky)
  love.graphics.draw(self.image, x, y, r, sx, sy, ox, oy, kx, ky)
end

function Texture:drawQuad(quad, x, y, r, sx, sy, ox, oy, kx, ky)
  love.graphics.draw(self.image, quad, x, y, r, sx, sy, ox, oy, kx, ky)
end

function Texture:drawMask(x, y, r, sx, sy, ox, oy, kx, ky)
  love.graphics.draw(self.mask, x, y, r, sx, sy, ox, oy, kx, ky)
end

function Texture:drawMaskQuad(quad, x, y, r, sx, sy, ox, oy, kx, ky)
  love.graphics.draw(self.mask, quad, x, y, r, sx, sy, ox, oy, kx, ky)
end

return Texture
