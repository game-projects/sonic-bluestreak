-- modules/assets :: a simple assets manager, aim to put every assets in a simple
-- serie of table in order to find them easily.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Assets = Object:extend()

local cwd  = (...):gsub('%.init$', '') .. "."

local Texture     = require(cwd .. "texture")

local Sprite      = require(cwd .. "sprites")
local Font        = require(cwd .. "fonts")
local ImageFont   = require(cwd .. "imagefonts")

local Tileset     = require(cwd ..  "tileset")
local Autotile    = require(cwd ..  "autotile")
local Background  = require(cwd ..  "background")

-- INIT FUNCTIONS
-- Initilizing and configuring option

function Assets:new()
  self:clear()

  self.isActive = true
end

function Assets:clear()
  -- TODO: destroy individually each texture/image when assets are cleared
  self:clearSprites()
  self:clearSFX()
  self:clearFonts()
  self:resetMusic()
  self:clearBackgrounds()
  self:clearFonts()
  self:clearTileset()

  self:clearImages()
end

function Assets:update(dt)
  if (self.isActive) then
    self:animationsUpdate(dt)
  end
end

-- IMPORT FUNCTIONS
-- Easilly import assets

function Assets:batchImport(datafile)
  local datas = require(datafile)

  for asset_type, assets in pairs(datas) do
    if (asset_type == "autotiles") then
      self:importAutotiles(assets)
    elseif (asset_type == "backgrounds") then
      self:importBackgrounds(assets)
    elseif (asset_type == "fonts") then
      self:importFonts(assets)
    elseif (asset_type == "imagefonts") then
      self:importImageFonts(assets)
    elseif (asset_type == "images") then
      self:importTextures(assets)
    elseif (asset_type == "sprites") then
      self:importSprites(assets)
    elseif (asset_type == "textures") then
      self:importTextures(assets)
    elseif (asset_type == "tilesets") then
      self:importTilesets(assets)
    elseif (asset_type == "sfx") then
      self:importSFX(assets)
    else
      core.debug:warning("assets/importer", "unkown asset type " .. asset_type)
    end
  end
end

function Assets:importAutotiles(assets)
  for i, asset in ipairs(assets) do
    self:addAutotile(asset[1], asset[2])
  end
end

function Assets:importBackgrounds(assets)
  for i, asset in ipairs(assets) do
    self:addBackground(asset[1], asset[2])
  end
end

function Assets:importFonts(assets)
  for i, asset in ipairs(assets) do
    self:addFont(asset[1], asset[2], asset[3])
  end
end

function Assets:importImageFonts(assets)
  for i, asset in ipairs(assets) do
    self:addImageFont(asset[1], asset[2], asset[3])
  end
end

function Assets:importSprites(assets)
  for i, asset in ipairs(assets) do
    self:addSprite(asset[1], asset[2])
  end
end

function Assets:importTextures(assets)
  for i, asset in ipairs(assets) do
    self:addImage(asset[1], asset[2])
  end
end

function Assets:importTilesets(assets)
  for i, asset in ipairs(assets) do
    self:addTileset(asset[1], asset[2])
  end
end

function Assets:importSFX(assets)
  for i, asset in ipairs(assets) do
    self:addSFX(asset[1], asset[2])
  end
end

-- SFX & MUSICS
-- Handle sound effects and musics

function Assets:addSFX(name, filepath)
  self:newSFX(name, filepath)
end

function Assets:newSFX(name, filepath)
  self.sfx[name] = love.audio.newSource( filepath, "static" )
end

function Assets:clearSFX()
  love.audio.stop( )
  self.sfx = {}
end

function Assets:playSFX(filename)
  if not (self.sfx[filename] == nil) then
    self.sfx[filename]:stop()
    self.sfx[filename]:setVolume(core.options.data.audio.sfx / 100)
    love.audio.play( self.sfx[filename] )
  end
end

function Assets:setMusic(filename)
  if filename ~= nil then
    love.audio.stop( )
    self.music = love.audio.newSource(filename, "stream" )
    self.music:setVolume(core.options.data.audio.music / 100)
  end
end

function Assets:silence()
  love.audio.stop()
end

function Assets:resetMusic()
  self.music = nil
end

function Assets:playMusic()
  if not (self.music == nil) then
    love.audio.play(self.music)
  end
end

-- IMAGES FUNCTIONS
-- Create directly texture items

function Assets:addImage(name, filename)
  self.images[name] = Texture(filename)
end

function Assets:drawImage(name, x, y, r, sx, sy, ox, oy, kx, ky)
  self.images[name]:draw(x, y, r, sx, sy, ox, oy, kx, ky)
end

function Assets:clearImages()
  self.images = {}
end

-- BACKGROUNDS FUNCTIONS
-- Automatic tiling texture

function Assets:addBackground(name, filepath)
  -- TODO: rework entirely background to work at any size
  self.backgrounds[name] = Background(filepath)
end

function Assets:clearBackgrounds()
  self.backgrounds = {}
end

-- SPRITES FUNCTIONS
-- Animated tileset

function Assets:addSprite(name, filepath)
  self.sprites[name] = Sprite(filepath)
end

function Assets:animationsUpdate(dt)
  for i,v in pairs(self.sprites) do
    v:update(dt)
  end
end

function Assets:clearSprites()
  self.sprites = {}
end

-- FONTS FUNCTIONS
-- Handles fonts and imagesfonts

function Assets:addFont(key, filename, size)
  local font = Font(filename, size)
  self.fonts[key] = font
end

function Assets:addImageFont(key, filename, extraspacing)
  local font = ImageFont(filename, extraspacing)
  self.fonts[key] = font
end

function Assets:getFont(filename)
  return self.fonts[filename]
end

function Assets:clearFonts()
  self.fonts = {}
end

-- TILESET FUNCTIONS
-- Automatically create quads for a texture

function Assets:addTileset(name, filepath)
  self.tileset[name] = Tileset(filepath)
end

function Assets:clearTileset()
  self.tileset = {}
end

-- AUTOTILE FUNCTIONS
-- Automatically draw tiles

function Assets:addAutotile(name, tilesize)
  self.autotile[name] = Autotile(name, tilesize)
end

function Assets:clearAutotile()
  self.autotile = {}
end

-- ACTIVITY FUNCTIONS
-- Handle activity

function Assets:setActivity(activity)
  self.isActive = activity
end

function Assets:switchActivity()
  self.isActive = (self.isActive == false)
end

function Assets:getActivity()
  return self.isActive
end

return Assets
