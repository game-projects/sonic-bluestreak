-- assets/fonts :: the imagefonts object, which are basically a bitmap version
-- of the font object.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.imagefonts$', '') .. "."

local Font        = require(cwd.. "fonts")
local ImageFont   = Font:extend()

-- INIT FUNCTIONS
-- Initilizing and configuring option

function ImageFont:new(filename, extraspacing)
  local data    = require(filename)
  local extraspacing = extraspacing or data.extraspacing or 1
  self.font   = love.graphics.newImageFont(filename .. ".png", data.glyphs, extraspacing)
  self.filter = ""
  self:setColor(1, 1, 1, 1)
  self:setSpacing(false, 0)
  self.align  = "left"
end

return ImageFont
