-- game :: The main game subsystem. Basically a big object that handle all the
-- game-related data like characters, monsters, etc. While the core aim to be
-- reusable at will, the game is specifically made for the current game.

-- It's also what handle the savedata for games

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.init$', '') .. "."
local cwd2 = (...):gsub('%.gamesystem.init$', '') .. "."

local GameSystem  = Object:extend()
local binser      = require(cwd2 .. "libs.binser")

local DEFAULT_SAVENUMBER = 3

function GameSystem:new()
  self.currentSlot = -1

  self.submodules = {}
  self.playtime = 0
  self:register()
end

function GameSystem:register()
  core:registerGameSystem(self)
end

function GameSystem:registerSubmodules(submodule)
  local name = submodule.name
  self.submodules[name] = submodule
end

-- UPDATE FUNCTIONS
-- Update every submodules

function GameSystem:update(dt)
  self.playtime = self.playtime + dt
  for k, submodule in pairs(self.submodules) do
    submodule:update(dt)
  end
end

-- DATA MANAGEMENT FUNCTIONS
-- Get and set data in the gamesystem object

function GameSystem:setData(data)
  local data = data
  self.playtime   = data.playtime
  for k, submodule in pairs(self.submodules) do
    submodule:setData(data[k])
  end
end

function GameSystem:getData()
  local data = {}
  data.playtime   = self.playtime
  for k, submodule in pairs(self.submodules) do
    data[k] = submodule:getData()
  end
  return data
end

-- SAVE MANAGEMENT FUNCTIONS
-- Get and set data in the gamesystem object

function GameSystem:getSaveNumber()
  return DEFAULT_SAVENUMBER
end

function GameSystem:resetSave(saveid)
  if utils.filesystem.exists("save" .. saveid .. ".save") then
    love.filesystem.remove( "save" .. saveid .. ".save" )
  end
end

function GameSystem:resetAllSaves()
  for i=1, self:getSaveNumber() do
    self:resetSave(i)
  end
end

function GameSystem:getSavePath(saveid, absolute)
  local saveDir = ""
  if (absolute) then
    saveDir = love.filesystem.getSaveDirectory() .. "/"
    if not utils.filesystem.exists(saveDir) then
      love.filesystem.createDirectory( "" )
    end
  end

  local filepath = saveDir .. self:getSaveName(saveid)

  return filepath
end

function GameSystem:getSaveName(saveid)
  return "save" .. saveid .. ".save"
end

function GameSystem:saveFileExist(saveid)
  return utils.filesystem.exists(self:getSaveName(saveid))
end

function GameSystem:read(saveid)
  self.currentSlot = saveid or self.currentSlot
  if (self.currentSlot > 0) then
    local savepath = self:getSavePath(self.currentSlot, true)
    if self:saveFileExist(self.currentSlot) then
      local datas = binser.readFile(savepath)

      self:setData(datas[1])
    end
  end
end

function GameSystem:write()
  if (self.currentSlot > 0) then
    local data = self:getData()

    savepath = self:getSavePath(self.currentSlot, true)
    binser.writeFile(savepath, data)
  end
end

return GameSystem
