local SubModule = Object:extend()

function SubModule:new(game, name)
  self.name = name or error("SUBMODULE must have a name")
  self.game = game
  self.data = {}

  self:register()
end

function SubModule:register()
  self.game:registerSubmodules(self)
end

function SubModule:getData()
  return self.data
end

function SubModule:setData(data)
  self.data = data
end

return SubModule
