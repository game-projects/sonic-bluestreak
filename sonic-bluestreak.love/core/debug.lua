-- core/debug.lua :: Debug functions for the core system.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local DebugSystem = Object:extend()

local cwd  = (...):gsub('%.debug$', '') .. "."
local lovebird = require(cwd .. "libs.lovebird")

function DebugSystem:new(controller, active)
  self.controller = controller
  lovebird.update()
  self.active = active or false
end

function DebugSystem:update(dt)
  lovebird.update(dt)
end

-- PRINT FUNCTIONS
-- Print and log debug string

function DebugSystem:print(context, string)
  if (self.active) then
    print("[DEBUG] ".. context .. ": " .. string)
  end
end

function DebugSystem:warning(context, string)
  if (self.active) then
    print("[WARNING] " .. context .. ": " .. string)
  end
end

function DebugSystem:error(context, string)
  if (self.active) then
    error("[ERROR] " .. context .. ": " .. string)
  end
end


return DebugSystem
