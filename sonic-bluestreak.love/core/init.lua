-- core/init.lua :: The main file of the core system, an object full of subsystem
-- loaded by the game to handle the main functions (like screen, translation,
-- inputs…)

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local cwd  = (...):gsub('%.init$', '') .. "."

-- GLOBAL UTILS/FUNCTION LOADING
-- Load in the global namespace utilities that'll need to be reusable everywhere
-- in the game

Object  = require(cwd .. "libs.classic")
utils   = require(cwd .. "utils")

local CoreSystem = Object:extend()

local DebugSystem   = require(cwd .. "debug")

local Options       = require(cwd .. "options")
local Input         = require(cwd .. "input")
local Screen        = require(cwd .. "screen")
local Lang          = require(cwd .. "lang")
local SceneManager  = require(cwd .. "scenemanager")

require(cwd .. "callbacks")

-- INIT FUNCTIONS
-- Initialize and configure the core object

function CoreSystem:new(DEBUGMODE)
  self.debug    = DebugSystem(self, DEBUGMODE)
  self.options  = Options(self)
  self.input    = Input(self)
  self.screen   = Screen(self)
  self.scenemanager = SceneManager(self)
  self.lang         = Lang(self)
end

function CoreSystem:registerGameSystem(gamesystem)
  self.game = gamesystem
end

-- MOUSE FUNCTIONS
-- get directly the mouse when needed

function CoreSystem:mousemoved(x, y, dx, dy)
  local x, y    = self.screen:project(x, y)
  local dx, dy  = self.screen:project(dx, dy)
  self.scenemanager:mousemoved(x, y, dx, dy)
end

function CoreSystem:mousepressed( x, y, button, istouch )
  local x, y    = self.screen:project(x, y)
  self.scenemanager:mousepressed( x, y, button, istouch )
end

-- KEYBOARD FUNCTIONS
-- get directly the keyboard when needed

function CoreSystem:keypressed( key, scancode, isrepeat )
  self.scenemanager:keypressed( key, scancode, isrepeat )
end

function CoreSystem:keyreleased( key )
  self.scenemanager:keyreleased( key )
end

-- UPDATE FUNCTIONS
-- Load every sytem every update functions of the scene and objects

function CoreSystem:update(dt)
  self.debug:update(dt)
  self.input:update(dt)

  if (self.game ~= nil) then
    self.game:update(dt)
  end

  self.scenemanager:update(dt)
end

-- DRAW FUNCTIONS
-- Draw the whole game

function CoreSystem:draw()
  self.scenemanager:draw()
end

-- EXIT FUNCTIONS
-- Quit the game

function CoreSystem:exit()
  self.options:save()
  love.event.quit()
end

return CoreSystem
