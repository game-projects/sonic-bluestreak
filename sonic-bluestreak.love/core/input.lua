-- core/input.lua :: The input system. This object take care of transforming the
-- differents inputs source in a virtual controller for the game.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local InputManager  = Object:extend()
local VirtualPad    = Object:extend()

-- INIT FUNCTIONS
-- Initialize and configure the controller system

function InputManager:new(controller)
  self.controller = controller
  self.data       = self.controller.options:getInputData()

  self:initSources()
end

function InputManager:initSources()
  self.sources = {}
  for sourceid, data in ipairs(self.data) do
    local source = VirtualPad(self, sourceid, data)
    table.insert(self.sources, source)
  end

  self.fakekeys = self:getKeyList(1)
end

-- INFO FUNCTIONS
-- Get functions from the controller object

function InputManager:isDown(sourceid, key)
  self.controller.debug:warning("core/input", "core.input:isDown is deprecated since 0.7.0 and will be removed in 0.8.0")
  return self.sources[sourceid]:isDown(key)
end

function InputManager:getKeyList(sourceid)
  local keys = {}
  if self.data[sourceid] ~= nil then
    for k,v in pairs(self.data[sourceid].keys) do
      keys[k]  = {}
      keys[k].isDown     = false
      keys[k].isPressed  = false
      keys[k].isReleased = false
    end
  end

  return keys
end

function InputManager:getKey(sourceid, padkey)
  local padkey = padkey
  for k,v in pairs(self.data[sourceid].keys) do
    if (k == padkey) then key = v end
  end
  return key
end

function InputManager:getSources()
  return self.sources
end

-- KEY MANAGEMENT FUNCTIONS
-- Manage pressed keys

function InputManager:flushKeys()
  for i, source in ipairs(self.sources) do
    source:flushKeys()
  end
end

function InputManager:flushSourceKeys(sourceid)
  self.keys = {}
  self.sources[sourceid]:flushKeys()
end

-- UPDATE FUNCTIONS
-- Check every step pressed keys

function InputManager:update(dt)
  for i, source in ipairs(self.sources) do
    source:checkKeys()
  end
end

------------------------------------ VIRTUALPADS -------------------------------
-- Virtual representation of a pad
-- The role of the virtualpad is to return all the data a controller at any time
-- They can be flushed and deactivated for a while when needed

-- INIT FUNCTIONS
-- Initialize and configure the controller system

function VirtualPad:new(controller, id, data)
  self.controller = controller
  self.id         = id
  self.data       = data

  self.type       = self.data.type or "nil"

  self:initKeys()
end

function VirtualPad:initKeys()
  local keys = {}
  if (self.data ~= nil) then
    for k,v in pairs(self.data.keys) do
      keys[k]  = {}
      keys[k].isDown     = false
      keys[k].isPressed  = false
      keys[k].isReleased = false
    end
  end

  self.keys     = keys
  self.fakekeys = keys
end

function VirtualPad:isDown(key)
  local isdown = false

  if self.type == "keyboard" then
    isdown = love.keyboard.isDown(self.data.keys[key])
  else
    local warnstring = "unsupported input device " .. self.type .. " for source " .. self.id
    core.debug:warning("core/input", warnstring)
  end

  return isdown
end

function VirtualPad:checkKeys()
  for key, keydata in pairs(self.keys) do
    self:checkKey(key)
  end
end

function VirtualPad:checkKey(key)
  local isDown = self:isDown(key)
  if (isDown) then
    if not (self.keys[key].isDown) then
      core.debug:print("virtualpad", "key " .. key .. " is Pressed")
      self.keys[key].isDown     = true
      self.keys[key].isPressed  = true
      self.keys[key].isReleased = false
    else
      if (self.keys[key].isPressed) then
        --core.debug:print("virtualpad", "key " .. key .. " is Down")
        self.keys[key].isPressed = false
      end
    end
  else
    if (self.keys[key].isDown) then
      self.keys[key].isDown     = false
      self.keys[key].isPressed  = false
      self.keys[key].isReleased = true
    else
      if (self.keys[key].isReleased) then
        core.debug:print("virtualpad", "key " .. key .. " is Released")
        self.keys[key].isReleased = false
      end
    end
  end
end

function VirtualPad:getKeys()
  return self.keys
end

function VirtualPad:getKey(key)
  return self.keys[key]
end

function VirtualPad:flushKeys()
  for key, _ in pairs(self.keys) do
    self:flushKey(key)
  end
end

function VirtualPad:flushKey(key)
  self.keys[key].isDown     = false
  self.keys[key].isPressed  = false
  self.keys[key].isReleased = false
end


return InputManager
