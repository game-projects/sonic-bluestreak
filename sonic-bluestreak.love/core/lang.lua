-- core/langs.lua :: The translation system. Transform a string to another
-- according to the translations files in the datas/ folder.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local LanguageManager = Object:extend()

local TRANSLATION_PATH = "datas/languages/"

-- INIT FUNCTIONS
-- Initialize and configure the translation system

function LanguageManager:new(controller)
  self.controller = controller

  self.data       = self:getTranslationData()
  self:setLang(self.controller.options.data.language.current)
end

function LanguageManager:setLang(lang)
  self.controller.options.data.language.current = lang
  self.lang = self.controller.options.data.language.current
end

-- INFO FUNCTIONS
-- Get informations from the translation manager

function LanguageManager:getCurrentLang()
  return self.data.language.current
end

function LanguageManager:getDefaultLang()
  return self.data.language.default
end

function LanguageManager:getTranslationData()
  return self.controller.options.data.language
end

function LanguageManager:getLangMetadata(lang)
  local langfilepath = self.data.path .. lang

  return require(langfilepath)
end

function LanguageManager:getLangName(lang)
  local metadata = self:getLangMetadata(lang)

  return metadata.name
end

function LanguageManager:getCurrentLangName()
  return self:getLangName(self.data.current)
end

function LanguageManager:isLangAvailable(lang)
  local isAvailable = false

  for i,v in ipairs(self.data.available) do
    if v == lang then
      isAvailable = true
    end
  end

  return isAvailable
end

-- TRANSLATION FUNCTIONS
-- get the translation of a string

function LanguageManager:getTranslationStringList(lang, library)
  local _path = self.data.path .. lang .. "/" .. library
  local fileinfo = love.filesystem.getInfo(_path .. ".lua")
  local list = nil

  if fileinfo ~= nil then
    list = require(_path)
  else
    core.debug:warning("core/lang","file " .. _path .. " do not exists")
  end

  return list
end

function LanguageManager:translateFromLang(lang, library, stringToTranslate)
  local _stringlist = self:getTranslationStringList(lang, library)

  if _stringlist == nil then
    return nil
  else
    return _stringlist[stringToTranslate]
  end
end

function LanguageManager:translate(library, string)
  local translation = self:translateFromLang(self.data.current, library, string)

  if (translation == nil) then
    translation = self:translateFromLang(self.data.default, library, string)
  end

  if (translation == nil) then
    translation = string
    core.debug:warning("core/lang", "no translation path found for " .. string .. " in " .. library)
  end

  return translation
end

return LanguageManager
