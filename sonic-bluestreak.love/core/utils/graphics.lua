-- loveutils.graphics : a set of useful functions for love2D. Aim to reduce
-- boilerplate in love2D by creating usefull function to handle these roles.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Graphics = {}

-- COLOR FUNCTIONS
-- Handle colors and scene colors

function Graphics.resetColor()
  love.graphics.setColor(1,1,1,1)
end

-- PRINT TEXT
-- Functions to draw text on screen

function Graphics.print(text, x, y, align, r, sx, sy, ox, oy, kx, ky)
  local width
  local font = love.graphics.getFont()
  width = font:getWidth(text)

  if align == "center" then
    width = (width/2)
  elseif align == "right" then
    width = width
  else
    width = 0
  end

  love.graphics.print(text, x - (width), y, r, sx, sy, ox, oy, kx, ky)
end

function Graphics.printWithSpacing(text, spacing, align, x, y, r, sx, sy, ox, oy, kx, ky)
  -- DO NOT USE THIS FUNCTION IN A "UPDATE" FUNCTION !
  -- it's pretty heavy to use as it use a loop to get every character in a text
  local font = love.graphics.getFont()
  local xx = 0
  local lenght = string.len(text)
  local basewidth = font:getWidth(text)
  local width = basewidth + (spacing * lenght)

  if align == "center" then
    width = (width/2)
  elseif align == "right" then
    width = width
  else
    width = 0
  end

  for i=1, lenght do
    local char =  string.sub(text, i, i)
    pos = math.floor(x + xx - width)
    love.graphics.print(char, pos, y)
    xx = xx + font:getWidth(char) + spacing
  end
end

-- PLACEHOLDER GRAPHICS FUNCTIONS
-- Ready-to-use placeolder and stuff

function Graphics.box(x, y, w, h)
  local x = math.floor(x)
  local y = math.floor(y)
  local w = math.floor(w)
  local h = math.floor(h)
  local a = a or 1

  local r, g, b, a = love.graphics.getColor( )

  love.graphics.setColor(r, g, b, 0.3 * a)
  love.graphics.rectangle("fill", x, y, w, h)

  love.graphics.setColor(r, g, b, a)
  love.graphics.rectangle("line", x, y, w, h)
end

return Graphics
