-- loveutils.math : easy to use functions for mathematics and geometry.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Math = {}

-- ALGEBRA FUNCTIONS
-- Simple yet usefull functions not supported by base love2D

function Math.sign(x)
   if (x < 0) then
     return -1
   elseif (x > 0) then
     return 1
   else
     return 0
   end
end

function Math.round(num)
  return math.floor(num + 0.5)
end

function Math.toZero(num, sub)
  local sub = math.floor(sub)

  if math.abs(num) < sub then
    return 0
  else
    return num - (sub * Math.sign(num))
  end
end

function Math.between(num, value1, value2)
  local min = math.min(value1, value2)
  local max = math.max(value1, value2)
  return math.min(math.max(num, min), max)
end

-- VECTOR/DIRECTION functions
-- Easy-to-use function to handle point and motion

function Math.vector(x1, y1, x2, y2)
  local vecx, vecy

  vecx = x2 - x1
  vexy = y2 - y1

  return vecx, vecy
end

function Math.getMiddlePoint(x1, y1, x2, y2)
  local newx, newy, vecx, vecy

  vecx = math.max(x1, x2) - math.min(x1, x2)
  vecy = math.max(y1, y2) - math.min(y1, y2)

  newx = math.min(x1, x2) + (vecx / 2)
  newy = math.min(y1, y2) + (vecy / 2)

  return newx, newy
end

function Math.pointDistance(x1, y1, x2, y2)
  local vecx, vecy

  vecx = math.max(x1, x2) - math.min(x1, x2)
  vecy = math.max(y1, y2) - math.min(y1, y2)

  return math.sqrt(vecx^2 + vecy^2)

end

function Math.pointDirection(x1,y1,x2,y2)
  local vecx, vecy, angle
  vecy = y2 - y1
  vecx = x2 - x1
  angle = math.atan2(vecy, vecx)

  return angle
end

-- STRING FUNCTIONS
-- Transform into string numbers

function Math.numberToString(x, length)
  local length = length or 1
  local string = ""
  local x = x
  if (x >= math.pow(10, length)) then
    x = unitsNumber*10 - 1
    string = string .. x
  else
    for i=1, (length-1) do
      if (x < math.pow(10, length-i)) then
        string = string .. "0"
      end
    end
    string = string .. x
  end
  return string
end

-- COORDINATE FUNCTIONS
-- Easy computation on coordinate

function Math.floorCoord(x, y)
  return math.floor(x), math.floor(y)
end

function Math.pixeliseCoord(x, y, factor)
  x, y = Math.floorCoord(x / factor, y / factor)

  x = x * factor
  y = y * factor

  return x, y
end

return Math
