-- scene.lua :: a basic scene management system, that work by sending the different
-- core functions to the scene, normally without the scene itself having to manage
-- them.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local SceneManager  = Object:extend()

-- INIT FUNCTIONS
-- Initialize and configure the scene manager

function SceneManager:new(controller)
  self.controller   = controller
  self.currentScene = nil

  self.storage      = {}
end

function SceneManager:setScene(scene)
  self.currentScene = scene
end

function SceneManager:storeCurrentScene(name)
  self.storage[name] = self.currentScene
end

function SceneManager:setStoredScene(name)
  local storedScene = self.storage[name]
  if storedScene ~= nil then
    self.currentScene   = storedScene
    self.storage[name]  = nil
  end
end

function SceneManager:clearStorage()
  self.storage = {}
end

function SceneManager:clearScene()
  self.currentScene = nil
end

-- UPDATE FUNCTIONS
-- Update the current scene and its subobjects

function SceneManager:update(dt)
  if (self.currentScene ~= nil) then
    self.currentScene:updateStart(dt)
    self.currentScene:setKeys()
    self.currentScene.assets:update(dt)
    self.currentScene.menusystem:update(dt)
    self.currentScene:updateWorld(dt)
    self.currentScene:update(dt)
    self.currentScene:updateEnd(dt)
  end
end

-- MOUSE FUNCTIONS
-- Send pointer data to the scene

function SceneManager:mousemoved(x, y, dx, dy)
  if (self.currentScene ~= nil) then
    self.currentScene.mouse.x,
    self.currentScene.mouse.y = x, y
    self.currentScene:mousemoved(x, y, dx, dy)
    self.currentScene.menusystem:mousemoved(x, y, dx, dy)
  end
end

function SceneManager:mousepressed( x, y, button, istouch )
  if (self.currentScene ~= nil) then
    self.currentScene:mousepressed( x, y, button, istouch )
    self.currentScene.menusystem:mousepressed( x, y, button, istouch )
  end
end

-- KEYBOARD FUNCTIONS
-- Add send keys functions to the scene

function SceneManager:keypressed( key, scancode, isrepeat )
  self.currentScene:keypressed( key, scancode, isrepeat )
end

function SceneManager:keyreleased( key )
  self.currentScene:keyreleased( key )
end

-- DRAW FUNCTIONS
-- Draw the current scene

function SceneManager:draw()
  self.controller.screen:apply()
  if (self.currentScene ~= nil) then
    self.currentScene:drawStart()
    self.currentScene:drawWorld()
    self.currentScene:draw()
    self.currentScene.menusystem:draw()
    self.currentScene:drawEnd()
  end
  self.controller.screen:cease()
end

return SceneManager
