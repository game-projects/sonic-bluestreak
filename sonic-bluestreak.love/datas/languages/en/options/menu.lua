return {
  ["options"] = "OPTIONS",
  ["video"]   = "VIDEO",
  ["audio"]   = "AUDIO",
  ["lang"]    = "LANGUES",
  ["input"]   = "CONTROLES",
  ["reset"]   = "RESET",
  ["exit"]    = "EXIT",
  ["back"]    = "BACK",
  ["sfx"]     = "SFX",
  ["music"]   = "MUSIC",
  ["keyboard"]  = "Clavier",
  ["inputtype"] = "SOURCE",
  ["vsync"]   = "VSYNC",
  ["borders"] = "BORDURES",
  ["fullscreen"]  = "PLEIN ECRAN",
  ["resolution"]  = "RESOLUTION"
}
