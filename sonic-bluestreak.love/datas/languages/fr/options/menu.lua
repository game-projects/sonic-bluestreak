return {
  ["options"] = "SETTINGS",
  ["video"] = "VIDEO",
  ["audio"] = "AUDIO",
  ["lang"] = "LANGUAGES",
  ["input"] = "CONTROLS",
  ["reset"] = "RESET",
  ["exit"] = "EXIT",
  ["back"] = "BACK",
  ["sfx"] = "SFX",
  ["music"] = "MUSIC",
  ["keyboard"] = "Keyboard",
  ["inputtype"] = "SOURCE",
  ["vsync"] = "VSYNC",
  ["borders"] = "BORDERS",
  ["fullscreen"] = "FULLSCREEN",
  ["resolution"] = "RESOLUTION"
}
