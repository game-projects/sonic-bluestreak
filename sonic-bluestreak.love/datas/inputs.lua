return {
    [1] = {
      type = "keyboard",
      keys = {
        ["left"]  = "left",
        ["right"] = "right",
        ["up"]    = "up",
        ["down"]  = "down",
        ["A"]     = "a",
        ["B"]     = "z",
        ["C"]     = "e",
        ["start"] = "return",
        ["select"] = "space"
      }
    }
  }
