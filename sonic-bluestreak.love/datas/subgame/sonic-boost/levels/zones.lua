return {
  ["forest"] = {
    name    = "Jade Forest",
    borders = 0,
    tiles   = 0,
    background = "forest",
    music = "1-01- Beyond The Speed Of"
  },
  ["city"] ={
    name    = "Diamond Highway",
    borders = 1,
    tiles   = 1,
    background = "city",
    music = "city"
  },
  ["tunnel"] ={
    name    = "Peridot Tunnel",
    borders = 2,
    tiles   = 2,
    background = "tunnel",
    music = nil
  },
  ["mountain"] ={
    name    = "Pearl Mountain",
    borders = 3,
    tiles   = 3,
    background = "mountain"
  },
  ["hills"] ={
    name    = "Calcite Hills",
    borders = 4,
    tiles   = 4,
    background = "hills"
  },
  ["bridge"] ={
    name    = "Carnelian Bridge",
    borders = 5,
    tiles   = 5,
    background = "bridge"
  },
  ["castle"] ={
    name    = "Ametist Castle",
    borders = 6,
    tiles   = 6,
    background = "castle",
    music = nil
  },
}
