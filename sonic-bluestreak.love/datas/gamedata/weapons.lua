return {
  [1] = {
    name = "basic",
    launch = {{0, 0, 0}},
    zspeed = 0,
    xspeed = 360,
    color = {1, 1, 1},
    explode = false,
    bounce = false,
  },
  [2] = {
    name = "fire",
    launch = {{0, 0, 0}},
    zspeed = 360,
    xspeed = 360,
    color = {1, 0.2, 0.2},
    explode = false,
    bounce = false,
  },
}
