return {
  acc = 0.2,  -- Basic acceleration
  dec = 0.5,  -- Basic deceleration
  grv = 1.3,  -- Gravity
  max = 4,    -- DEPRECATED: max speed
  jmp = 4,    -- DEPRECATED: jump initial velocity
  boost = 1.2,-- Boost value when you are dashing compared to your max speed.
  side = 2.5, -- sidestep speed
  spring = 4, -- Spring initial velocity
}
