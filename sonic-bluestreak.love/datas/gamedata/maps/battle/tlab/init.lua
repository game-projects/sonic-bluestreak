return {
  name = "Tails Lab",
  blocks    = {
    { 26,  24, 64, 32, "top1", "side1"},
    {122, 150, 64, 32, "top1", "side1"},
  },
  parallax  = {
    {0,   0,  .50,  0, true, false, "back1"},
    {0,   -32,    1,  1, true, false, "back2"},
  }
}
