return {
  name = "Holy Summit",
  blocks    = {
    {  0, 240, 288, 48, "top1", "side1"},
    {  0, 176, 176, 64, "top2", "noside"},
    {240, 176,  48, 64, "top3", "noside"},
    {  0, 144, 288, 32, "top4", "side1"},
    {  0,  48,  48, 96, "top5", "noside"},
    {144,  48, 144, 96, "top6", "noside"},
    {  0,   0, 288, 48, "top7", "side1"},
  },
  parallax  = {
    {0,  -48,  .5,  0, true, false, "back1"},
    {0,  -12,   1,  1, true, false, "back2"},
  }
}
