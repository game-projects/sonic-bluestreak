return {
  name = "Chao Ruins",
  blocks    = {
    { 56,  128, 16, 96, "top1", "side1"},
    {248,  128, 16, 96, "top1", "side1"},
    {112,   72-16, 96, 16, "top2", "side2"},
    {112,  264-16, 96, 16, "top2", "side2"},
  },
  parallax  = {
    {0,    0,  .4,  0, true, false, "back1"},
    {0,   79,   1,   1, true, false, "back2"},
    {0,  -28,   1,   1, true, false, "back3"},
  }
}
