return {
  [0] = {
    setDashTo     = nil,
    changeSpd     = false,
    max           = 1,
    acc           = 1,
    spd           = 0,
    groundbellow  = true,
    springPow     = 0,
    playSFX       = nil,
    hazard        = false,
  }
}
