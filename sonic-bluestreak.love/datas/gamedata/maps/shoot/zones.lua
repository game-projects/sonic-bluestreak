return {
  ["forest"] = {
    name    = "Jade Forest",
    tiles   = 4,
    background = "forest"
  },
  ["city"] ={
    name    = "Diamond Highway",
    tiles   = 1,
    background = "city"
  },
  ["tunnel"] ={
    name    = "Peridot Tunnel",
    tiles   = 2,
    background = "tunnel",
    music = nil
  },
  ["mountain"] ={
    name    = "Pearl Mountain",
    tiles   = 3,
    background = "mountain"
  },
  ["coast"] ={
    name    = "Olivine Coast",
    tiles   = 0,
    background = "hills"
  },
  ["bridge"] ={
    name    = "Carnelian Bridge",
    tiles   = 5,
    background = "bridge"
  },
  ["castle"] ={
    name    = "Ametist Castle",
    tiles   = 6,
    background = "castle",
    music = nil
  },
}
