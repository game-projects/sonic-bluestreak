return {
  name        = "Default",
  name_full   = "Default the Character",
  class       = "Speedster",
  startlevel  = 1,
  isUnlockedAtStart = true,

  base_stats  = {
    hpmax   = 200, --
    ppmax   = 50, --

    attack  = 50, --
    power   = 50, --
    defense = 50, --
    technic = 50, --
    mind    = 50, --
    speed   = 50, --

    turns   =  3, -- number of attacks by turn (unused)
    move    =  2, -- how far the character can get in one turn
  },

  color = {1, 1, 1},

  skill_list = {
  --{attack_name, level},
  },

  flags = {
    canGoSuper = true,
  },

  assets = {
    charset   = {"", 1},
    lifeicon  = 1,
    spriteset = "sonic",
  },

  inventory = {
    haveShoes = true,
    haveMechs = false,
    haveGlove = true,
    haveHammer= false,
    accessories_number = 3,
    chao_number = 1,
  },

  boost_stats = {
    spd = 5,
    jmp = 3,
    jumpaction = "doublejump",
    jumpaction_power = 2,
    action = "spinattack",
    action_power = 1,
    canBreakCraft = false,
  }
}
