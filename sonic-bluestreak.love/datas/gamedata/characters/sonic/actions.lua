local actions = {}

actions.aerial = {}
actions.special = {}

function actions.aerial.start(n, actor)

end

function actions.aerial.update(dt, actor)

end

function actions.aerial.onGround(actor)

end

function actions.special.start(n, actor)

end

function actions.special.update(dt, actor)

end

function actions.special.onGround(actor)

end

return actions
