return {
  startlevel = 1,
  hpmax   = 50, -- Les points de vie du personnages
  spd     = 5,
  jmp     = 3,
  atk     = 5,
  jumpaction_power = 2,
  action_power  = 1,
  action_cost = 30,

  canGoSuper        = true,
  canBreakCraft     = false
}
