-- game/characters :: The character handler. This object handle all the character
-- and is able to get and set datas about them.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local CharacterManager = Object:extend()

function CharacterManager:new(controller)
  self.controller = controller
  self.namelist = require "datas.gamedata.characters"
  self.list     = {}
  self.team     = require "datas.gamedata.characters.baseteam"
  self.active   = 1
  self:init()
end

function CharacterManager:init()
  for k, v in pairs(self.namelist) do
    local dir = "datas/gamedata/characters/" .. v .. "/init.lua"
    local fileinfo = love.filesystem.getInfo(dir)
    if fileinfo ~= nil then
      self:initCharacter(v)
    end
  end
end

function CharacterManager:getCharacterData(charname)
  -- va eprmettre de récupérer les données d'un personnage
  local charfolder = "datas.gamedata.characters." .. charname
  local character = require(charfolder)
  character.stats       = require(charfolder .. ".stats")
  character.assets      = require(charfolder .. ".assets")
  character.skills      = require(charfolder .. ".skills")
  character.actions     = require(charfolder .. ".actions")

  return character
end

function CharacterManager:initCharacter(id)
  local character = self:getCharacterData(id)

  local startlevel = character.stats.startlevel
  character.stats.level     = startlevel
  character.stats.exp       = self:getExpValue(startlevel)
  character.stats.exp_next  = self:getExpValue(startlevel + 1)

  character.stats.hp = character.stats.hpmax
  character.stats.pp = 100
  character.stats.status = 0

  self.list[id] = character
end

function CharacterManager:getExpValue(level)
  return math.floor( ( 4 * ( level ^ 3 ) ) / 5 )
end

function CharacterManager:setLevel(id, newlevel)
  self.list[id].stats.level = newlevel
  local stats = self.list[id].stats
  local exp, exp_next, exp_current
  exp         = self:getExpValue(stats.level)
  exp_next    = self:getExpValue(stats.level + 1)
  exp_current = self.list[id].stats.exp

  self.list[id].stats.exp = math.max(math.min(exp_current, exp_next - 1), exp)
  self.list[id].stats.exp_next = exp_next
end

function CharacterManager:levelUp(id)
  self:setLevel(id, self.list[id].stats.level + 1)
end

function CharacterManager:getStatValue(level, base)
  return math.floor( (((base * 2) * level)/100) ) + 5
end

function CharacterManager:getHPValue(level, base)
  return math.floor( (((base * 2.7) * level)/100) ) + 15 + level
end

function CharacterManager:getData()
  local data = {}
  data.list = self.list
  data.team = self.team
  return data
end

function CharacterManager:setData(data)
  local data = data
  self.list = data.list
  self.team = data.team
end

function CharacterManager:heal(id)
  self.list[id].stats.hp = self.list[id].stats.hpmax
  self.list[id].stats.pp = 100
  self.list[id].stats.status = 0
end

function CharacterManager:addToTeam(id)
 self:heal(id)
 table.insert(self.team, id)
end

function CharacterManager:removeToTeam(teamid)
  self.team[teamid] = ""
end

function CharacterManager:getActiveCharacter()
  return self.team[self.active]
end

-- DEBUG FUNCTIONS

function CharacterManager:printCharacter(id)
  local character = self.list[id]
  local stats = character.stats
  print(id .. ". " .. character.fullname)
  print("Lvl " .. character.stats.level .. " (" .. stats.exp .. "/" .. stats.exp_next .. " exp)")
end

function CharacterManager:printTeam()
  for i,v in ipairs(self.team) do
    self:printCharacter(v)
    print("-----")
  end
end

return CharacterManager
