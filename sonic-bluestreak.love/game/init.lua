-- game :: The main game subsystem. Basically a big object that handle all the
-- game-related data like characters, monsters, etc. While the core aim to be
-- reusable at will, the game is specifically made for the current game.

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Game        = Object:extend()
local Characters  = require "game.characters"
local Ennemies    = require "game.ennemies"

local binser      = require "core.modules.gamesystem.libs.binser"

Game.utils  = require "game.modules.utils"
Game.gui    = require "game.modules.gui"

function Game:new()
  self.slot = -1
  self.gametime = 0

  self.characters = Characters(self)
  self.ennemies   = Ennemies(self)
end

function Game:setData(data)
  local data = data
  self.gametime   = data.gametime
  self.characters:setData(data.characters)
end

function Game:getData()
  local data = {}
  data.gametime   = self.gametime
  data.characters = self.characters:getData()

  return data
end

function Game:read(save_id)
  self.slot = save_id
  if (self.slot > 0) then
    filepath = self:getSaveFile(self.slot, true)
    if love.filesystem.exists("save" .. self.slot .. ".save") then
      local loadedDatas = binser.readFile(filepath)

      self:setData(loadedDatas[1])

    end
  end
end

function Game:write(save_id)
  if (self.slot > 0) then
    local data = self:getData()

    filepath = self:getSaveFile(self.slot, true)
    binser.writeFile(filepath, data)
  end
end

function Game:getSaveFile(saveslot, absolute)
  local dir = ""
  if absolute then
    dir = love.filesystem.getSaveDirectory() .. "/"
    if not love.filesystem.exists(dir) then
      love.filesystem.createDirectory( "" )
    end
  end

  local filepath = dir .. "save" .. saveslot .. ".save"

  return filepath
end

function Game:resetSaves()
  for i=1, 3 do
    filepath = self:getSaveFile(i, true)
    if love.filesystem.exists("save" .. i .. ".save") then
      love.filesystem.remove( "save" .. i .. ".save" )
    end
  end
end

function Game:update(dt)
  self.gametime = self.gametime + dt
end

function Game:getTime()
  local time = self.gametime
  local hours, minute, seconds
  seconds = math.floor(self.gametime)
  minutes = math.floor(seconds / 60)
  hours = math.floor(minutes / 60)
  seconds = seconds % 60
  minutes = minutes % 60
  hours = hours

  return seconds, minutes, hours
end

function Game:getTimeString()
  local string
  local seconds, minutes, hours = self:getTime()
  local stringSeconds, stringMinutes, stringHours
  if (seconds <= 9) then
    stringSeconds = 0 .. seconds
  else
    stringSeconds = seconds
  end

  if (minutes <= 9) then
    stringMinutes = 0 .. minutes
  else
    stringMinutes = minutes
  end

  if (hours <= 9) then
    stringHours = 0 .. hours
  else
    stringHours = hours
  end

  string = stringHours .. ":" .. stringMinutes .. ":" .. stringSeconds
  return string
end

function Game:printTime()
  print(self:getTimeString())
end

return Game
