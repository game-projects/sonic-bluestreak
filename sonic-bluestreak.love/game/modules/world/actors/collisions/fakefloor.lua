local Base = require "core.modules.world.actors.actor3D"
local FakeFloor = Base:extend()

function FakeFloor:new(world, x, y, z, w, h, d)
  FakeFloor.super.new(self, world, "fakefloor", x, y, z, w, h, d, false)
  self:setDebugColor(0,0,0)
  self.boxes.Base(self, w, h, d, false)
end

function FakeFloor:update(dt)

end

function FakeFloor:draw()
  local drawx, drawy, drawz = self.x, self.y, self.z
  if (self.world.maptype == "shoot") then
    drawx = drawx + math.floor(drawy/2)
  end
  self:drawStart()
  if (self.box ~= nil) then
    self.box:draw(drawx, drawy, drawz)
  else
    local x, y = math.floor(drawx), math.floor(drawy - drawz - self.d + (self.h/2))
    self:drawSprite(x, y)
  end
  self:drawEnd()
end

return FakeFloor
