local Base = require "core.modules.world.actors.actor3D"
local Wall = Base:extend()

function Wall:new(world, x, y, z, w, h, d)
  Wall.super.new(self, world, "wall", x, y, z, w, h, d, true)
  self:setDebugColor(0,0,0)
  self.boxes.Base(self, w, h, d)
end

function Wall:update(dt)

end

return Wall
