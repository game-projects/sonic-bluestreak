local Base = require "core.modules.world.actors.actor3D"
local Textured = Base:extend()

function Textured:new(world, x, y, z, w, h, d, top, bottom)
  Textured.super.new(self, world, "wall", x, y, z, w, h, d, true)
  self:setDebugColor(0,0,0)
  self.boxes.Textured(self, w, h, d, top, bottom)
end

function Textured:update(dt)

end

return Textured
