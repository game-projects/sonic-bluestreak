local Base = require "core.modules.world.actors.actor3D"
local Invisible = Base:extend()

function Invisible:new(world, x, y, z, w, h, d)
  Invisible.super.new(self, world, "wall", x, y, z, w, h, d, true)
  self:setDebugColor(0,0,0)
end

function Invisible:update(dt)

end

return Invisible
