local Base = require "core.modules.world.actors.actor3D"
local Floor = Base:extend()

function Floor:new(world, x, y, z, w, h, d)
  Floor.super.new(self, world, "wall", x, y, z, w, h, d, true)
  self:setDebugColor(0,0,0)
  self.boxes.Base(self, w, h, d, false)
end

function Floor:update(dt)

end

function Floor:draw()
  local drawx, drawy, drawz = self.x, self.y, self.z
  if (self.world.maptype == "shoot") then
    drawx = drawx + math.floor(drawy/2)
  end
  self:drawStart()
  if (self.box ~= nil) then
    self.box:draw(drawx, drawy, drawz)
  else
    local x, y = math.floor(drawx), math.floor(drawy - drawz - self.d + (self.h/2))
    self:drawSprite(x, y)
  end
  self:drawEnd()
end

return Floor
