local Parent = require "game.modules.world.actors.parent"
local WeaponShot = Parent:extend()

function WeaponShot:new(world, x, y, z, direction, data)
  WeaponShot.super.new(self, world, "shot", x, y, z, 16, 8, 16, false)
  self:setSprite("ringweapon", 0, 0)
  self.direction = direction
  self.data = data
  self.zsp = self.data.zspeed
  self.xsp = self.data.xspeed * direction
  self.xfrc, self.yfrc = 0, 0
  if (self.zsp ~= 0) then
    self:setGravity(480*2)
  end
end

function WeaponShot:updateStart(dt)
  if (self.xsp == 0) then
    self:destroy()
  end
end

function WeaponShot:draw()
  love.graphics.setColor(self.data.color[1], self.data.color[2], self.data.color[3], 1)
  WeaponShot.super.draw(self)
  love.graphics.setColor(self.data.color[1], self.data.color[2], self.data.color[3], 0.6)
  local x, y = self.x + self.y/2 + 8, self.y - self.z - 3
  local angle = utils.math.pointDirection(0, 0, self.xsp, self.zsp * -1)
  self.scene.assets.sprites["ringtoss"]:drawAnimation(x, y, angle)
  love.graphics.setColor(1, 1, 1, 1)
end

return WeaponShot
