local cwd  = (...):gsub('%.ring$', '') .. "."
local Collectible = require(cwd .. "collectible")
local Ring = Collectible:extend()

function Ring:new(world, x, y, z)
  Ring.super.new(self, world, x, y, z+6, 16, 10, 16)
  self:setSprite("ring", 0, 0)
end

function Ring:getPicked(player)
  player:setRing(1, true)
  player:setScore(10, true)

  self:destroy()
end

return Ring
