local Parent = require "game.modules.world.actors.parent"
local Collectible = Parent:extend()

function Collectible:new(world, x, y, z, w, h, d)
  Collectible.super.new(self, world, "collectible", x, y, z, w, h, d, false)
end

function Collectible:getPicked(player)
  self:destroy()
end

return Collectible
