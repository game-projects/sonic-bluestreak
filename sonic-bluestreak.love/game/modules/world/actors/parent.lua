local Base = require "core.modules.world.actors.actor3D"
local Parent = Base:extend()

function Parent:new(world, type, x, y, z, w, h, d, isSolid)
  self.scene = world.scene
  Parent.super.new(self, world, type, x, y, z, w, h, d, isSolid)
end

function Parent:draw()
  local drawx, drawy, drawz = self.x, self.y, self.z
  if (self.world.maptype == "shoot") then
    drawx = drawx + math.floor(drawy/2)
  end
  self:drawStart()
  if (self.box ~= nil) then
    self.box:draw(drawx, drawy, drawz)
  else
    local x, y = math.floor(drawx), math.floor(drawy - drawz - self.d + (self.h/2))
    if (math.floor(drawz) < -2) and (self.world.maptype == "shoot") then
      core.screen:setScissor(0, 0, 424, 58+drawy/2)
      self:drawSprite(x, y)
      core.screen:resetScissor()
     else
      self:drawSprite(x, y)
    end
  end
  self:drawEnd()
end

function Parent:drawShadow(x, y)
  local x = x or 0
  local y = y or 0
if (self.world.maptype == "shoot") then
  x = x + math.floor(y/2)
end
  self.assets.images["shadow"]:draw(x-4, y+4)
end

return Parent
