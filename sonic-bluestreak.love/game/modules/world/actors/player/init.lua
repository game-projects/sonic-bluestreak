local cwd  = (...):gsub('%.player$', '') .. "."
local Parent = require(cwd .. "parent")
local Player = Parent:extend()

local Frame = require("game.modules.gui.frame")
local Statusbar = require("game.modules.gui.status")

local Weapons = require(cwd .. "player.weapons")

function Player:new(world, x, y, z, id)
  Player.super.new(self, world, "player", x, y, 0, 16, 16, 24, false)
  self:setGravity(480*2)

  self:init(id)

  self.frame = Frame()

  self.weapons = Weapons(self)

  self.action = "normal"

  self.rings = 0
  self.score = 0
end

function Player:init(id)
  self.playerID = id
  self.charName = self.scene:getCharacterName(self.playerID)
  self:setSprite(self.charName, 8, 10)
  self:cloneSprite()
  self.statusbar = Statusbar(self, "speed", 50)
end

function Player:updateStart(dt)
  self.xfrc, self.yfrc = 480*3, 480*3

  self:basicMovements()

  if self.keys["A"].isPressed and (self.onGround) then
    self.zsp = 280*1.33
  end

  if self.keys["B"].isPressed then
    self.weapons:shoot(self.x, self.y+1, self.z+8, 1)
  end
end

function Player:basicMovements()

  if self.keys["up"].isDown then
    self.ysp = -160
  end
  if self.keys["down"].isDown then
    self.ysp =  160
  end
  if (self.world.autorun == true) then
    self.xsp = 160
  else
    if self.keys["left"].isDown then
      self.xsp = -160
    end
    if self.keys["right"].isDown then
      self.xsp =  160
    end
  end

end

function Player:collisionResponse(collision)
  if collision.other.type == "collectible" then
    collision.other.owner:getPicked(self)
  end
end

function Player:animationEnded(name)

end

function Player:updateEnd(dt)
  self:setAnimation()
end

function Player:setAnimation()
  local gsp = utils.math.pointDistance(0, 0, self.xsp, self.ysp)
  self:setCustomSpeed(math.abs(gsp) / 12)
  self:setDirection(self.xsp)
  if (self.action == "punching") then
    --the animation system is already active
  else
    if (self.onGround) then
      if (math.abs(self.xsp) > 0) or (math.abs(self.ysp) > 0) then
        self:changeAnimation("walk", false)
      else
        self:changeAnimation("idle", false)
      end
    else
      if (self.zsp) > 0 then
        self:changeAnimation("jump", false)
      else
        self:changeAnimation("fall", false)
      end
    end
  end
end

function Player:setDirection(direction)
  direction = direction or 0
  if direction ~= 0 then
    direction = utils.math.sign(direction)
    self.direction = direction
    self:setSpriteScallingX(direction)
  end
end

function Player:getViewCenter()
  local x, y = Player.super.getViewCenter(self)
  return self.world:getViewCenter(x, y)
end

function Player:draw()
  Player.super.draw(self)
end

function Player:drawHUD(id)
  self.frame:draw()
  self.statusbar:draw(8, 12)
end

function Player:setRing(value, isRelative)
  if (isRelative == false) then
    self.rings = 0
  end
  self.rings = self.rings + value
  self.statusbar.rings = self.rings
end

function Player:setScore(value, isRelative)
  if (isRelative == false) then
    self.score = 0
  end
  self.score = self.score + value
end

return Player
