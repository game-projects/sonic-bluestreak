local WeaponManager = Object:extend()

local weaponList = require("datas.gamedata.weapons")

function WeaponManager:new(player)
  self.player = player
  self:switch(1)
end

function WeaponManager:switch(newWeapon)
  if (weaponList[newWeapon] ~= nil) then
    self.currentWeapon = newWeapon;
    self.player.statusbar:setWeapon(self.currentWeapon);
  end
end

function WeaponManager:shoot(x, y, z, dir)
  weaponData = weaponList[self.currentWeapon]

  for i,coord in ipairs(weaponData.launch) do
    self.player.obj.Weapon(self.player.world, x + coord[1], y + coord[2], z + coord[3], dir, weaponData)
  end
end

return WeaponManager
