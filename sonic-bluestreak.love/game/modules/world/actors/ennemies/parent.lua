local Parent = require "game.modules.world.actors.parent"
local EnnemyParent = Parent:extend()

function EnnemyParent:new(world, x, y, z, w, h, d)
  EnnemyParent.super.new(self, world, "ennemy", x, y, z, w, h, d, true)
end

function EnnemyParent:getHurt(hp)
  if (self.hp <= hp) then
    self:die()
  else
    self.hp = self.hp - hp;
  end
end

function EnnemyParent:die()
  self:destroy()
end
