local Obj = {}

-- On charge toutes les différentes types d'acteurs
local cwd  = (...):gsub('%.init$', '') .. "."
Obj.Player = require(cwd .. "player")
Obj.Ring = require(cwd .. "items.ring")
Obj.Weapon = require(cwd .. "weapons.parent")

Obj.index     = {}
Obj.index["player"]   = Obj.Player

Obj.collisions = {}
Obj.collisions["wall"]      = require(cwd .. "collisions.wall")
Obj.collisions["invisible"] = require(cwd .. "collisions.invisible")
Obj.collisions["floor"]     = require(cwd .. "collisions.floor")
Obj.collisions["textured"]  = require(cwd .. "collisions.textured")
Obj.collisions["fakefloor"]  = require(cwd .. "collisions.fakefloor")

Obj.index     = {}
Obj.index[01] = Obj.Ring
Obj.index[02] = Obj.Ring

return Obj
