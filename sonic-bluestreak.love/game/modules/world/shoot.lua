local ParentWorld = require "game.modules.world.parent"
local ShootWorld = ParentWorld:extend()

local customMap = require "game.modules.world.maps"

function ShootWorld:new(scene, mapname)
  local mappath = game.utils.getMapPath("shoot", mapname)
  ShootWorld.super.new(self, scene, "shoot", mapname)

  self.mapname = mapname
  self.autorun = true
end

function ShootWorld:getViewCenter(x, y)
  return x+128, y-16
end

function ShootWorld:createMapController()
  self.customMap.Shoot(self, self.maptype, self.mapname)
  self.cameras:lockY(10)
end

return ShootWorld
