local World3D = require "core.modules.world.world3D"
local ParentWorld = World3D:extend()

local customMap = require "game.modules.world.maps"

function ParentWorld:new(scene, maptype, mapname)
  local mappath = game.utils.getMapPath("test", "test")
  self.customMap = customMap

  ParentWorld.super.new(self, scene, "game.modules.world.actors", mappath, maptype)

  self.mapname = mapname
  self.autorun = false
end

function ParentWorld:createMapController()
  customMap.Test(self)
end

function ParentWorld:loadMapObjects()
  ParentWorld.super.loadMapObjects(self)
  self:addInvisibleWalls()
end

function ParentWorld:newObjFromIndex(id, x, y, z)
  self.obj.index[id](self, x, y, z)
end

function ParentWorld:addInvisibleWalls()
  local w, h = self:getDimensions()
  print(w, h)
  self.obj.collisions["invisible"](self,   0, -16,   0,  w,  16, 1000)
  self.obj.collisions["invisible"](self,   0,   h,   0,  w,  16, 1000)
  self.obj.collisions["invisible"](self,   w,   0,   0, 16,   h, 1000)
  self.obj.collisions["invisible"](self, -16,   0,   0, 16,   h, 1000)
end

function ParentWorld:getViewCenter(x, y)
  return x, y-16
end

function ParentWorld:draw(dt)
  self:drawBackgroundColor()
  local camNumber = self.cameras:getViewNumber()

  if (camNumber == 0) then
    self:drawMap()
    self:drawActors()
  else
    for i=1, camNumber do
      self:drawParallax(i)
      self.cameras:attachView(i)
      self:drawMap(i)
      self:drawActors(i)
      self.cameras:detachView(i)
    end
  end
end

function ParentWorld:drawMap(i)
  local x, y, w, h = self.cameras:getViewCoordinate(i)
  if (self.map ~= nil) then
    self.map:draw(x, y, w, h)
  end
end

function ParentWorld:drawParallax(i)
  local x, y, w, h = self.cameras:getViewCoordinate(i)
  if (self.map ~= nil) and (self.maptype ~= "sti") then
    self.map:drawParallax(x, y, w, h)
  end
end

return ParentWorld
