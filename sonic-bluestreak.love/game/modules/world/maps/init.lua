local customMap = {}

customMap.Test    = require "game.modules.world.maps.test"
customMap.Battle  = require "game.modules.world.maps.battle"
customMap.Shoot   = require "game.modules.world.maps.shoot"

return customMap
