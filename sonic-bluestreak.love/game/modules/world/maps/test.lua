local BaseMap = require "core.modules.world.maps.parent"
local TestMap = BaseMap:extend()

function TestMap:new(world)
  TestMap.super.new(self, world)
  --self:setPadding(0, 0, 0, 0)

  self.background = love.graphics.newImage("assets/backgrounds/dumbtestmap.png")
end

function TestMap:loadCollisions()
  local w, h = self:getDimensions()
  self.world:newCollision("floor", 0, 0,     -16, w, h, 16)
end

function TestMap:getDimensions()
  return self.background:getDimensions()
end

function TestMap:loadPlayers()
  self.world:addPlayer(16, 16, 0, 1)
end

function TestMap:loadActors()
  -- Empty Placeholder function
end

function TestMap:draw()
  -- Empty Placeholder function
  love.graphics.draw(self.background, 0, 0)
end

function TestMap:drawParallax(x, y, w, h)
  -- local imax, jmax = (w/32)+1, (h/32)+1
  -- local x, y = x or 0, y or 0
  -- local x = math.floor(x/4) % 32
  -- local y = math.floor((y+96)/6) % 32
  --
  -- for i=0, math.ceil(imax) do
  --   for j=0, math.ceil(jmax) do
  --     love.graphics.draw(self.background, (i-1)*32-x, (j-1)*32-y)
  --   end
  -- end
end

return TestMap
