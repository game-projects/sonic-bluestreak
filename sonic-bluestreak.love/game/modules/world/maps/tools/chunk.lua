local Chunk = Object:extend()
local ChunkTerrain = require "game.modules.world.maps.tools.chunkterrain"

function Chunk:new(map, data)
  self.map = map
  self.data = data

  self.grounds = {}
  self.areGroundsPrepared = false
end

function Chunk:getFakeData()
  local fakedata = {}
  local emptyline = {00, 00, 00, 00, 00, 00, 00, 00}
  fakedata.objects = {emptyline, emptyline, emptyline, emptyline, emptyline}
  fakedata.terrain = {emptyline, emptyline, emptyline, emptyline, emptyline}
  fakedata.grind   = {emptyline, emptyline, emptyline, emptyline, emptyline}

  return fakedata, false
end

function Chunk:update(dt)

end

function Chunk:spawnGrounds(x)
  if (self.areGroundsPrepared == false) then
    self:prepareGround()
  end
  self:generateGround(x)
end

function Chunk:prepareGround()
  for i=1, 5 do
    for j=1, 8 do
      if (self.data.terrain[i][j] ~= 3) and (self:haveNoGround(j, i)) then
        self:calculateGround(j, i)
        --self.map.world:newCollision("floor", x+(j-1)*31, (i-1)*20, -48, 31, 20, 48)
      end
    end
  end
  self.areGroundsPrepared = true
end

function Chunk:generateGround(basex)
  for i, ground in ipairs(self.grounds) do
    local x, y = ground.x, ground.y
    local w, h = ground.w, ground.h
    self.map.world:newCollision("invisible", basex+(x-1)*31, (y-1)*20, -48, 31*w, 20*h, 48)
  end
end

function Chunk:haveNoGround(x, y)
  for i, ground in ipairs(self.grounds) do
    if ground:isInside(x, y) then
      return false
    end
  end
  return true
end

function Chunk:addGround(x, y, w, h)
  table.insert(self.grounds,ChunkTerrain(x, y, w, h))
end

function Chunk:calculateGround(x, y)
  local groundHeight = 1
  local groundWidth = 1
  local maxHeight = 5-y
  local maxWidth = 8-x

  -- Creation de la première ligne
  local lineEmpty = true
  for i=0, maxWidth do
    if self:testTerrain(x + i, y) and (lineEmpty) then
      groundWidth = i + 1
    else
      lineEmpty = false
    end
  end

  -- Pour optimiser, on commence à la seconde ligne cette boucle
  for i=1, maxHeight do
    lineEmpty = true
    for j=0, (groundWidth-1) do
      if self:testTerrain(x + j, y + i) == false then
        lineEmpty = false
      end
    end
    if (lineEmpty) then
      groundHeight = i + 1
    else
      break
    end
  end

  self:addGround(x, y, groundWidth, groundHeight)
end

function Chunk:testTerrain(x, y)
  return (self.data.terrain[y][x] ~= 3) and (self:haveNoGround(x, y))
end

function Chunk:spawnObjects(x)
  for i=1, 5 do
    for j=1, 8 do
      local id = self.data.objects[i][j]
      if (id ~= 0) then
        self.map.world:newObjFromIndex(id, x+(j-1)*31+12, (i-1)*20, 0)
      end
    end
  end
end

function Chunk:draw(x)
  for i=1, 5 do
    for j=1, 8 do
      if (self.data.terrain[i][j] ~= 0) then
        local tiley = (i-1)*20
        local tilex = x + (j-1)*31 + (i-1)*10
        local tileid = self.data.terrain[i][j]

        self:drawTile(tilex, tiley, tileid)
      end
    end
  end
end

function Chunk:drawTile(x, y, type)
  self.map.world.scene.assets.tileset["sptiles"]:drawTile(type, x, y)
end

return Chunk
