local gui = {}

local barborder = love.graphics.newImage("assets/gui/status/barborder.png")

function gui.newBorder(width, height, middlePosition)
  local tileset   = love.graphics.newImage("assets/gui/system/borders.png")
  local tilequad  = {}
  local w, h = tileset:getDimensions()
  tilequad[1]     = love.graphics.newQuad(0, 0,  20, 20, w, h)
  tilequad[2]     = love.graphics.newQuad(20, 0, 20, 20, w, h)
  tilequad[3]     = love.graphics.newQuad(40, 0, 20, 20, w, h)
  tilequad[4]     = love.graphics.newQuad(60, 0, 20, 20, w, h)

  local Texture = love.graphics.newCanvas(width, height)

  love.graphics.setCanvas(Texture)
  utils.graphics.resetColor()

  local height  = math.ceil(height / 20)
  local width   = math.ceil(width  / 20)

  for i=1, width do
    if i < middlePosition then
      love.graphics.draw(tileset, tilequad[1], (i-1) * 20, 0)
    elseif (i == middlePosition) then
      love.graphics.draw(tileset, tilequad[2], (i-1) * 20, 0)
    else
      love.graphics.draw(tileset, tilequad[3], (i-1) * 20, 0)
    end

    if height > 1 then
      for j = 2, height do
        love.graphics.draw(tileset, tilequad[4], (i-1) * 20, (j-1) * 20)
      end
    end
  end

  love.graphics.setCanvas( )

  return Texture
end

function gui.drawBar(x, y, width, height)
  if (width > 0) then
    local height  = height or 7
    core.screen:setScissor(x, y, width, height)
    love.graphics.draw(barborder, x, y)
    local barwidth = math.max(width-14, 0)
    love.graphics.rectangle("fill", x+7, y, barwidth, height)
    love.graphics.draw(barborder, x+barwidth+7, y, 0, -1, -1, 7, 7)
    core.screen:resetScissor( )
  end
end

function gui.newTextBox(type, width, height)
  local filepath = "assets/gui/dialogs/" .. type .. ".png"
  local baseimage = love.graphics.newImage(filepath)
  local quad = {}
  quad[1] = love.graphics.newQuad(00, 00, 8, 8, 24, 24)
  quad[2] = love.graphics.newQuad(00, 08, 8, 8, 24, 24)
  quad[3] = love.graphics.newQuad(00, 16, 8, 8, 24, 24)
  quad[4] = love.graphics.newQuad(08, 00, 8, 8, 24, 24)
  quad[5] = love.graphics.newQuad(08, 08, 8, 8, 24, 24)
  quad[6] = love.graphics.newQuad(08, 16, 8, 8, 24, 24)
  quad[7] = love.graphics.newQuad(16, 00, 8, 8, 24, 24)
  quad[8] = love.graphics.newQuad(16, 08, 8, 8, 24, 24)
  quad[9] = love.graphics.newQuad(16, 16, 8, 8, 24, 24)
  local canvas  = love.graphics.newCanvas(width, height)

  love.graphics.setCanvas( canvas )


  for i=1, math.floor(width/8) do
    if (i == 1) then
      -- first line
      for j=1, math.floor(height/8) do
        if j == 1 then
          love.graphics.draw(baseimage, quad[1], (i-1)*8, (j-1)*8)
        elseif j == math.floor(height/8) then
          love.graphics.draw(baseimage, quad[3], (i-1)*8, (j-1)*8)
        else
          love.graphics.draw(baseimage, quad[2], (i-1)*8, (j-1)*8)
        end
      end
    elseif (i == math.floor(width/8)) then
      -- last line
      for j=1, math.floor(height/8) do
        if j == 1 then
          love.graphics.draw(baseimage, quad[7], (i-1)*8, (j-1)*8)
        elseif j == math.floor(height/8) then
          love.graphics.draw(baseimage, quad[9], (i-1)*8, (j-1)*8)
        else
          love.graphics.draw(baseimage, quad[8], (i-1)*8, (j-1)*8)
        end
      end
    else
      -- middle lines
      for j=1, math.floor(height/8) do
        if j == 1 then
          love.graphics.draw(baseimage, quad[4], (i-1)*8, (j-1)*8)
        elseif j == math.floor(height/8) then
          love.graphics.draw(baseimage, quad[6], (i-1)*8, (j-1)*8)
        else
          love.graphics.draw(baseimage, quad[5], (i-1)*8, (j-1)*8)
        end
      end
    end
  end

  love.graphics.setCanvas( )

  return canvas
end


return gui
