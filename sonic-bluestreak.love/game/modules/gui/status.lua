local StatusArea = Object:extend()

function StatusArea:new(character, type, hpmax)
  self.type = "e_" .. type;
  self.hpmax = hpmax;
  self.hp = hpmax;
  self.pp = 100;
  self.ppmax = 100;
  self.assets = character.assets
  self.weapon = 0
  self.rings = 0
end

function StatusArea:setWeapon(weapon)
  self.weapon = weapon
end

function StatusArea:setRings(rings)
  self.rings = rings
end

function StatusArea:setHp(hp)
  self.hp = hp
end

function StatusArea:setPp(pp)
  self.pp = pp
end

function StatusArea:draw(x, y)
  local x = x or 0
  local y = y or 0

  self.assets.images[self.type]:draw(x, y)
  if (self.weapon ~= 0) then
    self.assets.tileset["weapons"]:drawTile(self.weapon, x-2, y-2)
  end

  self.assets.images["statusbar"]:draw(x+8, y-2)

  local bar1 = math.floor((self.hp/self.hpmax)*107)
  local bar2 = math.floor((self.pp/100)*108)

  love.graphics.setColor(248/255, 160/255, 0, 1)
  game.gui.drawBar(x+25, y+9, bar1, 7)
  love.graphics.setColor(0, 248/255, 248/255, 1)
  game.gui.drawBar(x+13, y+21, bar2, 7)
  utils.graphics.resetColor()

  self.assets.fonts["smallnumbers"]:set()
  love.graphics.print(math.floor(self.hp) .. "/" .. self.hpmax, x+34, y+9)
  love.graphics.print(math.floor(self.pp) .. "%", x+34, y+21)

  self.assets.fonts["numbers"]:set()
  love.graphics.print(utils.math.numberToString(self.rings, 3), x+88, y-5)

  self.assets.images["hudring"]:draw(x+125, y-6)
end

return StatusArea
