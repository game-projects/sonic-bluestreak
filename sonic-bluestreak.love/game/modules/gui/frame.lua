local Frame = Object:extend()

function Frame:new()
  self.guiborder  = game.gui.newBorder(424, 20, 6)
  self.guiborder2 = game.gui.newBorder(424, 40, 12)
end

function Frame:draw()
  love.graphics.setColor(0.256, 0.632, 0.852, 1)
  love.graphics.rectangle("fill", 0, 220, 424, 20)
  utils.graphics.resetColor()
  love.graphics.draw(self.guiborder, 424, 20, 0, -1, -1)
  love.graphics.draw(self.guiborder2, 424, 220, 0,  1,  1, 424,  0)
  love.graphics.print(love.timer.getFPS(), 200, 8)
end

return Frame
