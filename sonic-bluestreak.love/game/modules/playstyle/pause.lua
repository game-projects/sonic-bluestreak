local ListBox   = require "core.modules.menusystem.listbox"
local PauseMenu = ListBox:extend()

local Widget = require "core.modules.menusystem.widgets"

local ResumeWidget    = Widget.Text:extend()
local RestartWidget   = Widget.Text:extend()
local ExitWidget      = Widget.Text:extend()

function PauseMenu:new(playstyle)
  local height, width, x, y
  height = 72
  width  = 128
  x = 424/2 -  width/2
  y = 240/2 - height/2

  PauseMenu.super.new(self, playstyle.menusystem, "pauseMenu", x, y, width, height, 3)
  self.playstyle = playstyle

  self:setSound(self.playstyle.assets.sfx["navigate"])
  self.isActive   = false
  self.isVisible  = false

  local font = self.playstyle.assets.fonts["menu"]

  self.textbox = game.gui.newTextBox("basic", width+16, height+16)

  ResumeWidget(self, font)
  RestartWidget(self, font)
  ExitWidget(self, font)
end

function PauseMenu:draw()
  love.graphics.draw(self.textbox, self.x-8, self.y-8)
  PauseMenu.super.draw(self)
end

--- MENU WIDGETS

function ResumeWidget:new(menu, font)
  ResumeWidget.super.new(self, menu, font, "resume")
end

function ResumeWidget:action()
  self.menu.isActive = false
  self.menu.isVisible = false
  self.menu.playstyle.world.isActive = true
  self.menu.playstyle.assets.isActive = true
  self.menu.playstyle:flushKeys()
end

function RestartWidget:new(menu, font)
  ResumeWidget.super.new(self, menu, font, "restart")
end

function RestartWidget:action()
  self.menu.playstyle:restartLevel()
  self.menu.isActive = false
  self.menu.isVisible = false
  self.menu.playstyle.world.isActive = true
  self.menu.playstyle.assets.isActive = true
  self.menu.playstyle:flushKeys()
end

function ExitWidget:new(menu, font)
  ExitWidget.super.new(self, menu, font, "exit")
end

function ExitWidget:action()
  self.menu.playstyle:exitLevel()
end


return PauseMenu
