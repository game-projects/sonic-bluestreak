Scene = require("core.modules.scenes")
local PlayStyle = Scene:extend()

local PauseMenu = require("game.modules.playstyle.pause")
local TestWorld = require("game.modules.world.parent")


function PlayStyle:new(supportedLevels, missionfile, playerList)
  local playerList = playerList or {"sonic"}

  PlayStyle.super.new(self)
  self.timer = 0
  self.assets:batchImport("game.modules.playstyle.assets")
  self:loadMissionFile(supportedLevels, missionfile)

  PauseMenu(self)

  self:initWorld()
  self:initMission()
  self:initCharacters(playerList)

  self:startLevel()
end

function PlayStyle:loadMissionFile(supportedLevels, missionfile)
  self.mission = require("datas.gamedata.missions." .. missionfile)
  self.assets:setMusic("assets/music/" .. self.mission.music)
end

function PlayStyle:initWorld()
  TestWorld(self)
end

function PlayStyle:initMission()
  -- NOTHING
end

function PlayStyle:initCharacters(characterList)
  self.characterList = characterList
  self.world:setPlayerNumber(#characterList)
  self.world.cameras:setMode("split")
  for i, characterName in ipairs(characterList) do
    self:initCharacterRessources(characterName)
  end
end

function PlayStyle:initCharacterRessources(characterName)
  local folder = "assets/sprites/characters/"
  self.assets:addSprite(characterName, folder .. characterName)
end

function PlayStyle:getCharacterName(charID)
  return self.characterList[charID]
end

function PlayStyle:update(dt)
  PlayStyle.super.update(self, dt)
    if self.menusystem.menus["pauseMenu"].isActive == false then
      self.timer = self.timer + dt
    end

    local keys = self:getKeys(1);

    if keys["start"].isPressed then
      if not (self.menusystem.menus["pauseMenu"].isActive) then
        self.menusystem.menus["pauseMenu"].isActive   = true
        self.menusystem.menus["pauseMenu"].isVisible  = true
        self.menusystem.menus["pauseMenu"]:getFocus()
        self.assets.isActive = false
        self.world.isActive = false
      else
        self.menusystem.menus["pauseMenu"].isActive   = false
        self.menusystem.menus["pauseMenu"].isVisible  = false
        self.assets.isActive = true
        self.world.isActive = true
      end
    end
end

function PlayStyle:startLevel()
  self.world:loadMap()
  self.assets:playMusic()
end

function PlayStyle:restartLevel()
  self.world:reset()
end

function PlayStyle:exitLevel()
  scenes.title()
end

return PlayStyle
