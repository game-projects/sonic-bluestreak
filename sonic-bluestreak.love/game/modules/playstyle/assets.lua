return {
  ["textures"] = {
    {"shadow", "assets/sprites/shadow.png"},
    {"e_power", "assets/gui/status/emblem_power.png"},
    {"e_fly", "assets/gui/status/emblem_technic.png"},
    {"e_speed", "assets/gui/status/emblem_speedster.png"},
    {"statusbar", "assets/gui/status/status_bar.png"},
    {"hudring", "assets/gui/hud/ring.png"}
  },
  ["sprites"] = {
    {"ring",      "assets/sprites/items/ring"},
    {"ringweapon",      "assets/sprites/items/ringweapon"},
    {"ringtoss",      "assets/sprites/items/ringtoss"}
  },
  ["imagefonts"] = {
    {"menu",    "assets/gui/fonts/SA2font"},
    {"numbers",    "assets/gui/fonts/numbers"},
    {"smallnumbers",    "assets/gui/fonts/smallnumbers"},
  },
  ["sfx"] = {
    {"navigate",  "assets/sfx/menu/select.wav"},
    {"confirm",   "assets/sfx/menu/validate.wav"},
    {"cancel",    "assets/sfx/menu/cancel.wav"},
  },
  ["tilesets"] = {
    {"weapons", "assets/gui/status/weapons"},
    {"sptiles", "assets/backgrounds/specialtile"}
  }
}
