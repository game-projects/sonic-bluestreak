local EnnemyManager = Object:extend()

function EnnemyManager:new(controller)
  self.controller = controller
end

function EnnemyManager:getEnnemyData(ennemy)
  local data = require("datas.gamedata.ennemies." .. ennemy)
  data.skills = require("datas.gamedata.ennemies." .. ennemy .. ".skills")
  data.stats  = require("datas.gamedata.ennemies." .. ennemy .. ".stats")

  return data
end

return EnnemyManager
