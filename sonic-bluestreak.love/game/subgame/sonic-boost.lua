local SonicBoost = Object:extend()

function SonicBoost:new(controller)
  self.controller = controller
  self.datas = {}

  self.datas.characters = require "datas.subgame.sonic-boost.characters"
end

function SonicBoost:getCharacterData(name)
  return self.datas.characters[name]
end

function SonicBoost:getData()
  return self.datas
end

return SonicBoost
