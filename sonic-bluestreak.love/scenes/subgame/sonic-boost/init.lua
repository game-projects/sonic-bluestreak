local Scene       = require "core.modules.scenes"
local BoostLevel  = Scene:extend()

local folder  = "scenes.subgame.sonic-boost.controller."

local HUD           = require(folder .. "hud")
local Background    = require(folder .. "background")
local Camera        = require(folder .. "camera")
local World         = require(folder .. "world")
local PauseMenu     = require(folder .. "pause")

local CharacterManager = require(folder .. "characters")

local zoneDatas    = require "datas.subgame.sonic-boost.levels.zones"

local LoadAssets  = require(folder .. "loader")

function BoostLevel:new()
  BoostLevel.super.new(self)
  self:initMission()

  LoadAssets(self)

  self:initManagers()

  self:resetFont()

  self.levelid = nil
  self.character = nil

  if self.datas.music ~= nil then
    local filepath = "assets/music/" .. self.datas.music .. ".mp3"
    print(filepath)
    self.assets:setMusic(filepath)
    self.assets:playMusic()
  end

  self.initiated  = false

  self.world:loadWorld()

  self.characters:newPlayer("sonic", 2)
  --self.characters:newDummy("sonic", 0)
  --self.characters:newDummy("sonic", 1)
  --self.characters:newDummy("sonic", 3)
  --self.characters:newDummy("sonic", 4)

  if self.areInputsLocked == nil then
    self.areInputsLocked = false
  end

  self:register()
end

function BoostLevel:initManagers()
  --self.loader     = Loader()
  self.world      = World(self)
  self.hud        = HUD(self)
  self.camera     = Camera(self, 0, 0)
  self.background = Background(self)
  self.characters = CharacterManager(self)

  PauseMenu(self)
end

function BoostLevel:initMission(levelid)
  self:getLevelData(levelid)

  self.mission = {}
  self.mission.timer      = 0
  self.mission.completed  = false
end

function BoostLevel:startMission()
  self.initiated = true
end

function BoostLevel:getLevelData(file)
  local file  = file or "testlevel.test1"
  local level = require("datas.subgame.sonic-boost.levels." .. file)

  self.zone = level.datas.zone

  local zone_data   = zoneDatas[self.zone]
  local bypass_data = level.datas.bypass_data

  self.datas = {}

  if (level.datas.bypass_zone) then
    self.datas.name       = bypass_data.name        or zone_data.name
    self.datas.borders    = bypass_data.borders     or zone_data.borders
    self.datas.tiles      = bypass_data.tiles       or zone_data.tiles
    self.datas.background = bypass_data.background  or zone_data.background
    self.datas.music      = bypass_data.music       or zone_data.music
  else
    self.datas = zone_data
  end

  self.layout         = level.layout
  self.parts          = level.parts
  self.endless_parts  = level.endless_parts

  self.missiondata        = level.datas.missiondata

end

function BoostLevel:update(dt)
  if self.menusystem.menus["pauseMenu"].isActive == false then
    self.assets:update(dt)
    self.mission.timer = self.mission.timer + dt
    self.world:update(dt)
    self.camera:update(dt)
  end

  local keys = self:getKeys(1);

  if keys["start"].isPressed then
    if not (self.menusystem.menus["pauseMenu"].isActive) then
      self.menusystem.menus["pauseMenu"].isActive   = true
      self.menusystem.menus["pauseMenu"].isVisible  = true
      self.menusystem.menus["pauseMenu"]:getFocus()
      self.assets.isActive = false
    else
      self.menusystem.menus["pauseMenu"].isActive   = false
      self.menusystem.menus["pauseMenu"].isVisible  = false
      self.assets.isActive = true
    end
  end
end

function BoostLevel:lockInputs()
  self.areInputsLocked = true
end

function BoostLevel:draw()
  self.background:draw()

  self.camera.view:attach()
  self.world:draw()
  self.camera.view:detach()

  utils.graphics.resetColor()
  self.hud:draw()
end

function BoostLevel:resetFont()
  self.assets.fonts["text"]:set()
end

function BoostLevel:restartLevel()
  self:new(self.levelid, self.character)
end

function BoostLevel:exitLevel()
  scenes.title()
end

function BoostLevel:leave()
  self.world:destroy()


  self.world      = nil
  self.hud        = nil
  self.camera     = nil
  self.background = nil
  self.characters = nil
  --self.pausemenu  = nil

  self.assets:clear()

  collectgarbage()
end

return BoostLevel
