local folder = "scenes.subgame.sonic-boost.actors."

local Entity        = require(folder .. "parent")
local Crystal = Entity:extend()

function Crystal:new(world, x, y, z)
  local z = z or 8
  Crystal.super.new(self, world, "collectible", x-8, y-8, z, 16, 16, 16)

  self:setSprite("crystal", 8, 16, true)

  self:setDebugColor(255, 255, 0)

  self.depth = 1
end

function Crystal:pickedUp(player)
  player:addScore(10)

  self:destroy()
end

return Crystal
