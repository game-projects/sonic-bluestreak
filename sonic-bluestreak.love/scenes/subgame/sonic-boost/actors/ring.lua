local folder = "scenes.subgame.sonic-boost.actors."

local Entity        = require(folder .. "parent")
local Ring = Entity:extend()

function Ring:new(world, x, y, z)
  local z = z or 8
  Ring.super.new(self, world, "collectible", x-8, y-8, z, 16, 16, 16)

  self:setSprite("ring", 8, 16, true)

  self:setDebugColor(255, 255, 0)

  self.depth = 1
end

function Ring:pickedUp(player)
  player.rings = player.rings + 1
  player:addScore(10)

  self:destroy()
end

return Ring
