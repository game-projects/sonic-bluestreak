local ActorIndex = {}
local folder = "scenes.subgame.sonic-boost.actors."

local Actor = folder .. "actors"

ActorIndex[1] = Actor.Ring
ActorIndex[2] = Actor.Crystal

return ActorIndex
