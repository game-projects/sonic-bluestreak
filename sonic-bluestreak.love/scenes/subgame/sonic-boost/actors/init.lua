local Actor = {}

local folder = "scenes.subgame.sonic-boost.actors."

Actor.Character = require(folder .. "character")
Actor.Ring      = require(folder .. "ring")
Actor.Crystal   = require(folder .. "crystal")
Actor.Rail      = require(folder .. "rail")

Actor.Index     = {}
Actor.Index[01] = Actor.Ring
Actor.Index[02] = Actor.Crystal

return Actor
