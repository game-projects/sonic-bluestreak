local folder = "scenes.subgame.sonic-boost.actors."

local Entity        = require(folder .. "parent")

function Block:new(level, x, y, z, w, h, d) -- On enregistre une nouvelle entité, avec par défaut sa hitbox.
  Block.super.new(self, level, "block", x-16, y-10, 0, 31, 20, d)
  self.name = "block"

  self:setDebugColor(255, 0, 0)
end

return Block
