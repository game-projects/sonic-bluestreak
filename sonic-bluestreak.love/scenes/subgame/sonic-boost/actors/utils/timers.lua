local Timer = Object:extend()

function Timer:new(entity, name, t)
  self.time = t
  self.entity = entity
  self.name = name
end

function Timer:update(dt)
  self.time = self.time - dt
  if (self.time <= 0) then
    self:finish()
  end
end

function Timer:finish()
  self.entity:endedTimer(self.name)
  self.entity.timers[self.name] = nil
end

return Timer
