local folder = "scenes.subgame.sonic-boost.actors."

local Entity        = require(folder .. "parent")
local Rail = Entity:extend()

function Rail:new(world, x, y, z, id)
  local z = z or 32
  self.railid = id or 2

  Rail.super.new(self, world, "grind", x, y-4, 24, 31, 8, 8)

  self:setDebugColor(.2, .2, .2)

  self.depth = 1
end

function Rail:draw()
  utils.graphics.resetColor( )
  love.graphics.draw(self.world.textures.rail, self.world.quads.rails[self.railid], self.x + math.floor(self.y / 2), self.y - self.z -(self.d / 2))
end

function Rail:drawEcho()
  utils.graphics.resetColor( )
  love.graphics.draw(self.world.textures.rail, self.world.quads.rails[self.railid], self.x + math.floor(self.y / 2) + self.world.width, self.y - self.z -(self.d / 2))
end

return Rail
