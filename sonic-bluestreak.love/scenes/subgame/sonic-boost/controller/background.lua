local Background = Object:extend()

function Background:new(controller, levelname)
  self.controller = controller
  local filename    = self.controller.datas.background or "forest"
  local backfolder  = "assets/backgrounds/sideview/parallax/"
  filename    = backfolder .. filename
  self.back1  = love.graphics.newImage(filename .. "-back.png")
  self.back2  = love.graphics.newImage(filename .. "-fore.png")
end

function Background:destroy()
  self.back1:release( )
  self.back2:release( )
end

function Background:draw()
  local x0, x1, x2

  local turn = self.controller.camera.turn

  x0 = self.controller.camera.view.x + (turn * self.controller.world.width)
  x1 = x0 / 3 % 240
  x2 = x0 / 9 % 480

  local sx = 1
  for i=1, 4 do
    if (i == 2) or (i == 4) then
      love.graphics.draw(self.back1, (i)*240 - x2, 20, 0, -1, 1)
    else
      love.graphics.draw(self.back1, (i-1)*240 - x2, 20, 0, 1, 1)
    end
  end

  for i=1, 3 do
    love.graphics.draw(self.back2, (i-1)*240 - x1, 20, 0, 1, 1)
  end

  --self:drawBorders()
  --self:drawGround(0, 90)
end

function Background:drawGround(x, y)
  for i=1, 5 do
    for j=1, 16 do
      local k = 1 + ((i + j) % 2)
      print(k)
      love.graphics.draw(self.normalTileImage, self.normalTile[k], x + (j-3)*31 + (i-1)*10 , y + (i-1)*20)
    end
  end
end

return Background
