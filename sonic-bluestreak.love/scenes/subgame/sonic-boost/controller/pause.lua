local ListBox   = require "core.modules.menusystem.listbox"
local PauseMenu = ListBox:extend()

local Widget = require "core.modules.menusystem.widgets"

local ResumeWidget    = Widget.Text:extend()
local RestartWidget   = Widget.Text:extend()
local ExitWidget      = Widget.Text:extend()

function PauseMenu:new(controller)
  local height, width, x, y
  height = 72
  width  = 72
  x = 424/2 -  width/2
  y = 240/2 - height/2

  PauseMenu.super.new(self, controller.menusystem, "pauseMenu", x, y, width, height, 3)
  self.controller = controller

  self:setSound(self.controller.assets.sfx["select"])
  self.isActive   = false
  self.isVisible  = false

  local font = self.controller.assets.fonts["menu"]

  ResumeWidget(self, font)
  RestartWidget(self, font)
  ExitWidget(self, font)
end

function PauseMenu:draw()
  PauseMenu.super.draw(self)
end

--- MENU WIDGETS

function ResumeWidget:new(menu, font)
  ResumeWidget.super.new(self, menu, font, "resume")
end

function ResumeWidget:action()
  self.menu.isActive = false
  self.menu.isVisible = false
  self.menu.controller.assets.isActive = true
  self.menu.controller:lockInputs()
end

function RestartWidget:new(menu, font)
  ResumeWidget.super.new(self, menu, font, "restart")
end

function RestartWidget:action()
  self.menu.controller:lockInputs()
  self.menu.controller:restartLevel()
end

function ExitWidget:new(menu, font)
  ExitWidget.super.new(self, menu, font, "exit")
end

function ExitWidget:action()
  self.menu.controller:exitLevel()
end


return PauseMenu
