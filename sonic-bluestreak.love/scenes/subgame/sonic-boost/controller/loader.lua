local Loader = Object:extend()

function Loader:new(controller)
  self.controller = controller
  self.assetQueue = {}
  self.pointer    = 1
  self.finished   = false
end

function Loader:addElement(type, name, filepath, arg1, arg2, arg3, arg4)
  local queueElement = {}

  queueElement.type = type
  queueElement.name = name
  queueElement.file = filepath
  table.insert(self.assetQueue, queueElement)

  self.finished = false
end

function Loader:update(dt)

end

function Loader:treatAssetQueueElement(id)
  if self.assetQueue[id] ~= nil then
    local asset = self.assetQueue[id]
    if asset.type == "image" then
      self.controller.assets:addImage(asset.name, asset.file)
    elseif asset.type == "sprite" then
      self.controller.assets:addSprite(asset.name, asset.file)
    elseif asset.type == "sfx" then
      self.controller.assets:addSFX(asset.name, asset.file)
    end
  end
end

function Loader:treatAssetQueue()
  for i = self.pointer, 10 do
    if i <= #self.assetQueue then
      self.finished = true
    end
  end
  self.pointer = i
end

local function LoadAssets(controller)
  controller.assets:addImage("shadow", "assets/sprites/shadow.png")
  controller.assets:addSprite("ring", "assets/sprites/items/ring")
  controller.assets:addSprite("crystal", "assets/sprites/items/crystal")

  controller.assets:addFont("text", "assets/fonts/PixelOperator.ttf", 16.5)
  --controller.gui:addTextBox("solid", "assets/gui/dialogbox.png")

  controller.assets:addSFX("ring", "assets/sfx/gameplay/objects/ring.wav")
  controller.assets:addSFX("booster", "assets/sfx/gameplay/objects/booster.wav")
  controller.assets:addSFX("jumpboard", "assets/sfx/gameplay/objects/jumpboard.wav")

  controller.assets:addSFX("jump", "assets/sfx/gameplay/actions/jump-modern.wav")
  controller.assets:addSFX("doublejump", "assets/sfx/gameplay/actions/doublejump.wav")
  controller.assets:addSFX("spin", "assets/sfx/gameplay/actions/spin.wav")

  controller.assets:addSFX("grind", "assets/sfx/gameplay/grind/grind.wav")
  controller.assets:addSFX("grindland", "assets/sfx/gameplay/grind/land.wav")

  controller.assets:addFont("menu", "assets/fonts/RussoOne-Regular.ttf", 15)
  controller.assets.fonts["menu"]:setAlign("center")
  controller.assets.fonts["menu"]:setFilter("border")
  controller.assets.fonts["menu"]:setSpacing(true, 1)

  controller.assets:addFont("title", "assets/fonts/Teko-Bold.ttf", 22)
  controller.assets.fonts["title"]:setAlign("center")
  controller.assets.fonts["title"]:setFilter("doubleborder")
  controller.assets.fonts["title"]:setSpacing(true, 2)

  controller.assets:addSFX("select", "assets/sfx/menu/select.wav")
  controller.assets:addSFX("validate", "assets/sfx/menu/validate.wav")
  controller.assets:addSFX("cancel", "assets/sfx/menu/cancel.wav")

end

return LoadAssets
