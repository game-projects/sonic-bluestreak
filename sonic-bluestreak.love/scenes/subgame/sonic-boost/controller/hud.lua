local BattleHUD = Object:extend()

local gui         = require "game.modules.gui"

function BattleHUD:new(controller)
  self.controller = controller
  self.target = nil

  self:loadAssets()
end

function BattleHUD:setTarget(target)
  self.target = target
end

function BattleHUD:loadAssets()
  self.progressbarImage  = love.graphics.newImage("assets/gui/hud/progressbar.png")
  self.barBack = love.graphics.newQuad(0, 0, 211, 12, 211, 24)
  self.barFore = love.graphics.newQuad(0, 12, 211, 12, 211, 24)
  self.hud1     = gui.newBorder(424, 20, 14)
  self.hud2     = gui.newBorder(424, 40, 12)

  self.ring     = love.graphics.newImage("assets/gui/hud/ring.png")



  self.font = love.graphics.newImageFont("assets/gui/hud/numbers.png", " 0123456789:/", 1)
  self.font2 = love.graphics.newImageFont("assets/gui/hud/smallnumbers.png", " 0123456789", 0)

  self.time   = love.graphics.newImage("assets/gui/hud/time.png")
  self.score  = love.graphics.newImage("assets/gui/hud/score.png")
  self.bonus  = love.graphics.newImage("assets/gui/hud/bonus.png")

  self.controller.assets:addTileset("lifeicon", "assets/sprites/characters/charicons")
end

function BattleHUD:destroy( )
  self.progressbarImage:release( )
  self.barBack:release( )
  self.barFore:release( )
  self.hud1:release( )
  self.hud2:release( )
end

function BattleHUD:update(dt)
  --
end

function BattleHUD:draw()
  love.graphics.setFont( self.font )
  self:drawFrame()

  if self.target ~= nil then
    self:drawTop(0, 0)
    self:drawBottom(0, 200)
  end

  self.controller:resetFont( )
end

function BattleHUD:drawFrame()
  love.graphics.setColor(0.256, 0.632, 0.852, 1)
  love.graphics.rectangle("fill", 0,   0, 424, 20)
  love.graphics.rectangle("fill", 0, 200, 424, 20)
  utils.graphics.resetColor()
  love.graphics.draw(self.hud1, 0,   0, 0,  1, -1,   0, 20)
  love.graphics.draw(self.hud2, 0, 200, 0, -1,  1, 424,  0)
end

function BattleHUD:drawTop(x, y)

  local ringx, ringy = x + 289,   y + 6
  love.graphics.draw(self.ring, ringx, ringy)
  local rings = utils.math.numberToString(self.target.rings, 3)

  love.graphics.print(rings, ringx + 14, ringy + 1)


  love.graphics.draw(self.score, x+8, y+3)
  local score = utils.math.numberToString(self.target.score, 9)
  local bonus = utils.math.numberToString(self.target.bonus, 3)
  local combo = utils.math.numberToString(self.target.combo, 3)

  love.graphics.print(score, x + 8 + 42, y + 4)

  local bonusx, bonusy = x + 165, y + 1
  love.graphics.draw(self.bonus, bonusx, bonusy)
  love.graphics.setFont( self.font2 )
    love.graphics.print(combo, bonusx + 28, bonusy)
    love.graphics.print(bonus, bonusx + 28, bonusy + 8)
  love.graphics.setFont( self.font )

  self.controller.assets.tileset["lifeicon"]:drawTile(self.target.lifeicon, x + 360, y + 4)
  local lives = utils.math.numberToString(self.target.life, 2)
  love.graphics.print(lives, x + 360 + 18, y + 7 )
end

function BattleHUD:drawBottom(x, y)
  local progressx, progressy = x + 204, y + 4
  love.graphics.draw(self.progressbarImage, self.barBack, progressx, progressy)
  local width = math.floor((self.target.x / self.controller.world.width) * (211 - 16))
  width = math.max(0, width)
  width = math.min(width, (211 - 16))
  love.graphics.setScissor(progressx + 8, progressy, width, 12)
  love.graphics.draw(self.progressbarImage, self.barFore, progressx, progressy)
  love.graphics.setScissor( )
  self.controller.assets.tileset["lifeicon"]:drawTile(self.target.lifeicon, progressx + width, progressy - 2)
  if (self.controller.missiondata.turns > 1) then
    local turn1 = utils.math.numberToString(self.target.turn, 2)
    local turn2 = utils.math.numberToString(self.controller.missiondata.turns, 2)
    love.graphics.printf(turn1 .. "/" .. turn2, progressx, progressy + 12 + 2, 210, "right")
  end

  local timex, timey = x + 5, y + 2
  love.graphics.draw(self.time, timex, timey)
  local timestring = self:getTimeString(self.controller.mission.timer)

  love.graphics.print(timestring, timex + 32, timey + 1)


end

function BattleHUD:getTimeString(numberOfSeconds)
  local numberOfSeconds = numberOfSeconds or 0
  local microseconds = math.floor((numberOfSeconds % 1) * 100)
  local seconds = math.floor(numberOfSeconds % 60)
  local minutes = math.floor(numberOfSeconds / 60)
  return   utils.math.numberToString(minutes, 2) .. ":"
        .. utils.math.numberToString(seconds, 2) .. ":"
        .. utils.math.numberToString(microseconds, 2)
end

return BattleHUD
