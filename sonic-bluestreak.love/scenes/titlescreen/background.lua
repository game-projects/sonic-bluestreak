local Background = Object:extend()

local zoneList = require("datas.gamedata.maps.shoot.zones")

function Background:new()
  self.x1 = 0
  self.x2 = 0
  self.x3 = 0
  self.y = 14

  local randomZoneList = {}
  for k,v in pairs(zoneList) do
    table.insert(randomZoneList, k)
  end
  local id = math.ceil(love.math.random() * #randomZoneList)
  self.zone = randomZoneList[id]
  print(self.zone)
  self:initRessources()
end

function Background:initRessources()
  self.textureId = zoneList[self.zone].tiles
  self.background = zoneList[self.zone].background

  self.texture = {}
  self.texture.floor  = self:generateFloor(self.textureId)
  self.texture.border = love.graphics.newImage("assets/backgrounds/borders.png")
  self.quads = {}
  local w, h = self.texture.border:getDimensions()
  self.quads.borders = love.graphics.newQuad(0, self.textureId*10, 80, 10, w, h)
  self:addParallax(self.background)
end

function Background:generateFloor(tile)
  local canvas = love.graphics.newCanvas(31*16, 100)
  local tile    = tile or 1

  local tileTexture = love.graphics.newImage("assets/backgrounds/normaltile.png")
  local tileQuad    = {}
  local w, h  = tileTexture:getDimensions()
  tileQuad[1] = love.graphics.newQuad( 0, tile*24, 40, 24, w, h)
  tileQuad[2] = love.graphics.newQuad(40, tile*24, 40, 24, w, h)

  love.graphics.setCanvas( canvas )

  for i=1, 5 do
    for j=0, 18 do
      local tiley = (i-1)*20 - 4
      local tilex = (j-2)*31 + (i-1)*10
      local variant = 1 + ((i + j) % 2)
      love.graphics.draw(tileTexture,   tileQuad[variant], tilex, tiley)
    end
  end

  love.graphics.setCanvas( )

  local imagedata = canvas:newImageData()
  local texture   = love.graphics.newImage( imagedata )
  imagedata:release()
  canvas:release()
  return texture
end

function Background:addParallax(filename)
  local filename    = filename or "forest"
  local backfolder  = "assets/backgrounds/parallax/"
  filename    = backfolder .. filename
  self.texture.back1  = love.graphics.newImage(filename .. "-back.png")
  self.texture.back2  = love.graphics.newImage(filename .. "-fore.png")
end

function Background:update(dt)
  local w, h = core.screen:getDimensions()
  self.x1 = (self.x1 + 10*dt)
  self.x2 = (self.x2 + 20*dt)
  self.x3 = (self.x3 + 30*dt)
end

function Background:draw()
  self:drawTiled(self.texture.back1, self.x1, 0, false)
  self:drawTiled(self.texture.back2, self.x2, 95, true)
  self:drawQuadTiled(self.texture.border, self.quads.borders, self.x3, 95, false)
  self:drawTiled(self.texture.floor, self.x3, 105, false)
  self:drawQuadTiled(self.texture.border, self.quads.borders, self.x3, 205, false)

end

function Background:drawTiled(drawable, x, y, fromBottom)
  local w, h = drawable:getDimensions()
  local screewn, _ = core.screen:getDimensions()
  local x = math.floor(x % w)

  local fromBottom = fromBottom or false
  local oy = 0
  if (fromBottom) then
    oy = h
  end

  local imax = math.ceil(screewn / w) + 1
  for i=0, imax do
    love.graphics.draw(drawable, (i*w) - x, self.y + y, 0, 1, 1, 0, oy)
  end
end

function Background:drawQuadTiled(drawable, quad, x, y, fromBottom)
  local w, h = drawable:getDimensions()
  local screewn, _ = core.screen:getDimensions()
  local x = math.floor(x % w)

  local fromBottom = fromBottom or false
  local oy = 0
  if (fromBottom) then
    local oy = h
  end

  local imax = math.ceil(screewn / w) + 1
  for i=0, imax do
    love.graphics.draw(drawable, quad, (i*w) - x, self.y + y, 0, 1, 1, 0, oy)
  end
end

return Background
