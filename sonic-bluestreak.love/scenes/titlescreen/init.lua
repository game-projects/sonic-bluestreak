-- scenes/test :: a basic test scene

--[[
  Copyright © 2019 Kazhnuz

  Permission is hereby granted, free of charge, to any person obtaining a copy of
  this software and associated documentation files (the "Software"), to deal in
  the Software without restriction, including without limitation the rights to
  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
  the Software, and to permit persons to whom the Software is furnished to do so,
  subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]

local Scene       = require "core.modules.scenes"
local TitleScreen = Scene:extend()
local gui         = require "game.modules.gui"

local TweenManager = require "game.modules.tweenmanager"
local Background = require "scenes.titlescreen.background"

function TitleScreen:new()
  TitleScreen.super.new(self)

  self.borders = gui.newBorder(424, 30, 8)
  self.assets:addImage("sonic", "assets/artworks/titlescreen_sonic.png")
  self.assets:addImage("logo", "assets/artworks/logo.png")
  self.assets:addImageFont("menu", "assets/gui/fonts/SA2font")

  self.tweens = TweenManager(self)
  self.background = Background(self)

  self.borderY  = 0
  self.logoX    =  270
  self.sonicX   = -180

  self.darkenOpacity = 1
  self.flashOpacity = 0
  self.canShowPressStart  = false
  self.showPressStart     = true
  self.showPressStartTimer = 0.5

  self.tweens:newTween(0.2, 0.5,  {borderY = 30}, "inOutQuart")
  self.tweens:newTween(0.5, 0.4, {darkenOpacity  = 0}, "outExpo")
  self.tweens:newTween(0.7, 0.6,  {logoX   = 0}, "inOutQuart")
  self.tweens:newTween(0.7, 0.6,  {sonicX  = 0}, "inOutQuart")
  self.tweens:newTween(1.3, 0.03, {flashOpacity  = 1}, "inQuart")
  self.tweens:newTween(1.45, 0.2, {flashOpacity  = 0}, "outExpo")
  self.tweens:newSwitch(1.4, { "canShowPressStart" })
  self:register()
end

function TitleScreen:update(dt)
  self.tweens:update(dt)
  self.background:update(dt)
  if (self.canShowPressStart) then
    self.showPressStartTimer = self.showPressStartTimer - dt
    if self.showPressStartTimer < 0 then
      self.showPressStart = (self.showPressStart == false)
      self.showPressStartTimer = 0.5
    end
  end
end

function TitleScreen:draw()
  utils.graphics.resetColor()
  self.background:draw()

  love.graphics.setColor(0, 0, 0, self.darkenOpacity)
  love.graphics.rectangle("fill", 0, 0, 424, 240)
  utils.graphics.resetColor( )

  love.graphics.draw(self.borders,   0,    self.borderY, 0,  1, -1)
  love.graphics.draw(self.borders,   424, 240 - self.borderY, 0, -1,  1)

  self.assets:drawImage("sonic", 90 + self.sonicX, 128, 0, 1, 1, 92, 106)
  self.assets:drawImage("logo", 290 + self.logoX,   60, 0, 1, 1, 150, 71)

  if (self.canShowPressStart) and (self.showPressStart) then
    local w = self.assets.fonts["menu"]:getWidth("PRESS START")
    self.assets.fonts["menu"]:print("PRESS START", 424/1.5, 240/1.5, "center", 0, 1, 1)
  end

  love.graphics.setColor(1, 1, 1, self.flashOpacity)
  love.graphics.rectangle("fill", 0, 0, 424, 240)
  utils.graphics.resetColor( )
end

return TitleScreen
