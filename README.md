# Sonic Bluestreak

## Principe de base

Sonic Bluestreak est un projet de remake/réimagination de ([Sonic Boom](https://www.youtube.com/watch?v=jYT28un2pEY)), ainsi que son remake ([Neo Sonic Boom](https://www.youtube.com/watch?v=MSofjiB_4J8&t=577s)). Ce jeu se veut une combinaison de ce fangame avec d'autres sources d'éléments ainsi qu'une réflexion sur ce qui étaient bien trouvé et intéressant sur ces vieux fangames.

Le jeu reprendra donc les grandes bases des deux jeux cité plus haut :
- **Vue orthognale "de côté"** : Le jeu utilisera une vue orthogonale de côté (un peu comme Sonic Battle mais sans l'aspect 3D).
- **Shooter** : vos personnages tireront des anneaux (comme dans Neo Sonic Boom) pour se battre. Des types d'anneaux spéciaux représenteront les différentes "armes".
- **Autorun** : Au moins une partie des niveaux utiliseront un système d'auto-run.

Une partie des niveaux proviendra également de ce jeu.
