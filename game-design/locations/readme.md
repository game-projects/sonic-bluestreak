## Levels in Sonic Boost game mode

## 1. Niveaux de Shadow Shoot

En bon jeu de flemmard, je reprend les 7 niveaux de Shadow Shoot :D Ces jeux serviront comme base aux jeux, et les niveaux supplémentaires à les améliorer.

### Diamound Highway

Un niveau de ville, à la manière de City Escape et Mission Street (ainsi que l'act 3/Knuckles de Speed Highway). Il représente une ville pleine d'énergie et de vitalité.

**Gimmicks** : Barre de grinds

### Peridot Tunnel

Un niveau de base sous-marine, à la manière de Ocean et Aquatic Bases (SAdv3 et Sonic).

**Gimmicks** : Espace sous-marin(?)

### Jadeite Forest

Un niveau de forêt, à la manière de Green Forest. Sert de premier niveau dans le monde inspiré de Neo Sonic Boom.

**Gimmicks** : Hautes herbes (qui ralenti)


### Pearl Mountain

Un niveau de neige, à la manière de ceux qu'on trouve dans les Advances.

**Gimmicks** : Glace (le personnage glisse sur les côté), snowboard(?)

### Calcite Hill

Un niveau de collines vertes, à la manière de Green Hill Zone.

**Gimmicks** : Eau

### Carnelian Bridge

Un nouveau de pont/route, à la manière de Radical et Speed Highway de SA et SA2. Aura quelques inspirations aussi de Starlight Zone.

**Gimmicks** :

### Ametist Castle

Le niveau de ruine du jeu, sera ici plus un chateau que des ruines "classic". Aura tout l'attirail des ruines : les trucs qui se cassent sous vos pieds, etc.

**Gimmicks** : Colonne (obstacle haut, indestructible), sol qui s'effondre

## 2. Niveaux supplémentaires
Le but des niveaux complémentaires va être de contenir les tropes qui ne sont pas présents dans les niveaux de bases du jeu. Il y aura 9 niveaux, pour atteindre un total de 16 niveaux (4×4 pour le mode "tournois).

### Zircon Desert

Un niveau de désert à la Pyramid Base et Sandopolis, offrant des décors de ruines "égyptiennes".

**Gimmicks** : Sable mouvant (on s'y enfonce progressivement + ralentissement)

### Graphit Mechanics

Une base d'Eggman classic, à la Secret Base, Chemical Plant.

**Gimmicks** :

### Silicon Matrix

Un niveau numérique, à la manière de Mad Matrix, Digital Circuit, Cyber Track and Techno Base.

**Gimmicks** :

### Moonstone Canyon

Un niveau de ruines/canyon venteux, à la manière de Windy Valley, Angel Island et Sky Canyon.

**Gimmicks** : Vents (ralentissant ou poussant tout les ennemis, s'activent ou se désactivent avec des boutons), ventilateurs sur le sol

### Obsidian Station

Une station spatiale, à la manière du Death Egg et de l'ARK.

**Gimmicks** :

### Agate Park

Un park/cirque/casino. Mélange des éléments des trois dans un éclat d'amusement et de perte d'argent.

**Gimmicks** : Jetons de Bingo/Série de 5 trucs à avoir

### Garnet Crater

De la lave. Du magma. Un peu comme Hot Crater et tout les niveaux du genre.

**Gimmicks** : Magma


### Lithium Metropolis

La ville moderne "lumineuse", à la manière de Grand Metropolis et Monopole. Aura quelques inspirations aussi de Metropolis de Sonic Forces. Ce sera une ville blanche, de lumière.

**Gimmicks** :

### Crystal Cosmos

Une route dans l'espace, pleine de lumière et d'énergie. Inspirée de la [Milky Way](https://www.youtube.com/watch?v=JhI9H_S85WE) et des niveaux espaces des jeux Sonic.

**Gimmicks** : Astéroides de couleurs différentes avec différents effets

## Correspondance de niveaux (mode classique)

- Niveau 1 : **Jadeite Forest** / **Calcite Hill**
- Niveau 2 : **Diamound Highway** / **Citrine Park**
- Niveau 3 : **Peridot Tunnel** / **Zircon Desert**
- Niveau 4 : **Pearl Mountain** / **Carnelian Bridge**
- Niveau 5 : **Graphit Mechanics** / **Moonstone Canyon**
- Niveau 6 : **Garnet Crater** / **Silicon Matrix**
- Niveau 7 : **Obsidian Station** / **Ametist Castle**
- Niveau Bonus : **Crystal Cosmos**

## Coupes (mode tournoi)

- Chao Cup : Calcite Hill - ????? - ????? - ?????
- Ring Cup : ????? - ????? - ????? - ?????
- Emerald Cup : ????? - ????? - ????? - ?????
- Master Cup : ????? - ????? - ????? - **Crystal Cosmos**
