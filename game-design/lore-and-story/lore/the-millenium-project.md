# Backstory : Le projet Gosth Island

## Le projet Millenium, le rêve d’une Utopie

Le projet Gosth Island était l’un des projets fondé par le « projet Millenium », un projet qui visait à construire partout sur la planète des « villes du futur ». Parmi les autres villes fondées par ce projet, il y a Monopole, Metal City et Grand Metropolis. Ce projet était financé par Rimlight et les Fédérations Unis, et tout un tas d’autres acteurs privés. Les Fédérations Unis avaient pour but d’offrir un monde meilleurs à tout les habitants de la planète, et empêcher à nouveau les guerres d’intervenir. Et le but de ces villes étaient d’illustrer la meilleure vie qu’ils auraient tous.

Ils se sont intéressé à Gosth Island, parce qu’ils estimaient que cette nouvelle énergie présente sur l’île serait un moyen de créer une ville comme jamais vu auparavant, qui dépasserait toute les autres, surtout s’ils réussissaient à faire venir avec eux le peuple qui justement « contrôlait » pour GUN la faille dont provenait l’énergie.

## L’achat de Gosth Island

Rimlight s’est donc rendu chez les Sidh et leur a proposé un deal : la coalition pour le projet pourrait travailler sur le Stygian Rift, à condition d’offrir une compensation financière à tout les membres du peuple qui accepteraient. Ils pourraient alors soit aller vers un autre de leur village, le Sidh Village, soit participer à la construction de la cité du futur et des installations scientifiques.

Le conseil réfléchi plusieurs jours, dans des débats extrêmement tendu. Sur les chefs, une partie étaient pour accepter l’offre, une partie contre. Contre toute attente, c’est la fille de l’un des membres du conseil et une apprentie-prêtresse qui fit basculer la balance en rejoignant elle-même le projet, voulant participer à cette création d’une utopie. Son argumentation fit que quatre des membres du conseil furent pour la vente de l’île. Trois encore refusaient :

- Benelos, un grand guerrier, qui estimait Ys Gradlon indigne de confiance, et qui refusait de vendre un territoire sacré à des humains. Il estimait que même pour une vie meilleurs, c’était une insulte envers leur membre, et qu’il fallait chercher ailleurs les ressources dont ils manquaient cruellement.

-  Morrigan, la prêtresse de la Stygian Rift estimait qu’utiliser comme une source d’énergie quelque chose d’aussi instable les faisait courir à la catastrophe. L’énergie Stygienne était par nature difficile à contrôler, et son aspect corrupteur aurait sans doute des effects catastrophique sur l’utopie.
Cependant, Dahut réussi à convaincre les autres que les risques étaient minimes par rapport à leur situation actuelle : Elle mis en valeur le faible tôt d’alphabétisation de leur population, la mortalité infantile très élevée. La jeune femme estimait qu’ils réussiraient avec leur force à préserver leur civilisation dans la modernité, comme la plupars des autres royaumes mobiens. Elle voulait utiliser la modernité comme un moyen pour les Sidh de continuer à exister, parce qu’elle estimait que simplement l’ignorer serait juste faire une politique de l’autruche jusqu’à ce qu’il serait trop tard pour eux et se ferait engloutir parce qu’ils n’auraient plus le choix.

- Lug, le plus jeune des membres du conseil, qui estimait que les contreparties étaient très faible, en vue du fait qu'avant l'arrivée des humains sur l'île, ils étaient maître de toutes l'ile.

Pendant plusieurs jours, les débats devinrent de plus en plus violent, la situation étant bloqué.

Les débats montèrent en intensité jusqu’à ce que Morrigan et Benelos furent radié du conseil pour les empêcher de continuer à poser leur véto, et exclu avec ceux qui les suivaient de la compensation financière. Ce fut le père de Dahut qui signa le traité avec les Fédérations Unis, ouvrant le début du projet Gosth Island. Il émigra dans le Feral Village pour assurer l’ordre dans son peuple, tandis que les autres chefs décidèrent de rester avec plusieurs Sidh pour participer aux travaux. Ceux qui étaient contre les travaux durent partir aussi, mais sans avoir le droit à une meilleure vie.

## Des débuts prometteurs mais sous les tensions

Les travaux commencèrent avec pas mal de succès dans les premiers temps. Ils réussirent à extraire et convertir l’énergie de la faille, et la centrale fut rapidement opérationnelle. Dahut gagna rapidement un rôle important, puisqu’elle était celle qui connaissait le mieux le fonctionnement de la faille. L’énergie était plus forte que tout ce que Rimlight avait prévu, et ils comprirent qu’ils pourraient allimenter toute la région avec. L’avancement de la ville se passait bien, et la construction d’un dôme capable de recréer des climats impressionnait les masses.

Dans la population de la ville, ce choix fut très décrier, et une partie du conseil du s'en aller de la ville principal, ou ne restèrent que Lug, Benelos et Morrigan pour diriger la ville. Un nouveau village fut entièrement financé par Rimlight pour celles et ceux qui voudraient collaborer avec eux.

Ils commencèrent à construire une route vers le Feral Village, pour les alimenter aussi, après des négociations du père de Dahut qui estimait que puisqu’ils allaient réussir plus, le peuple Sidh méritait aussi plus. Ce fut accepter, et le Dahut commença à jouir d’une réputation d’excellent négociateur. Mais une négociation était bien plus difficile, celle avec les membres de l'ancien village. Ceux-ci avaient cristalisé l’opposition, pour qu’elle continue à exister. Ils continuèrent d’adresser des critiques au projet, et commencèrent a tenter d’alerter la population générale des deux villages sur la situation de leur peuple.

Ys Gradlon, commandant du GUN, rejoint alors le projet pour pacifier la situation. Sa stratégie consistait à essayer de diviser le peuple Sidh entre les pour et les contre, pour pouvoir isoler les contre et éviter leur sentiment de s’étendre.
Cette situation dura plusieurs mois, dans un statut quo où d’un côté le projet avançait bien, mais où les critiques étaient toujours plus présente.

Cependant, un incident eut lieu. Un jeune guerrier, Albius, s’était infiltré dans la centrale pour faire un coup d’éclat. Il fut blessé par balle et plongé dans la faille, directement dans le Stygian Realm. Personne ne sut si le soldat avait fait exprès de jeter le jeune adulte blessé dans la faille ou si c'était un accident. Mais c’était un acte d’une violence incroyable pour le peuple Sidh, ce qui provoqua des troubles, et surtout la colère de Morrigan et de Benelos.

La guerre avait été déclarée.

## La bataille de Gosth Island

Une nuit, Benelos attaqua l’ile avec une armée de soldat. Leur but était simple : reprendre leur territoire. La bataille fit rage pendant des heures, et Benelos alla s’attaquer directement à Ys Gradlon, qui avait décidé de garder la faille. Le combat entre les deux fut violent, Ys Gradlon compensant sa faiblesse physique face au mobien bien plus fort par la supériorité de ses armes. En dehors, les soldats du G.U.N. et les guerriers Sidh se battait avec une férocité jamais vue.
La bataille fut remportée par GUN, et les guerriers Sidh furent fait prisonnier. Bénélos cependant n’eut pas cette chance, sombrant lors du combat dans la faille. Ys Gradlon tenta de sauver son adversaire, voulant lui offrir un procès équitable, mais en vain. Quant à Morrigan, elle tomba au combat, laissant la faille sans prêtre qualifié pour la première fois depuis des siècles.

Le peuple Sidh ne retrouva jamais des forces de batailles pour affronter GUN. Cependant, cet événement eut des conséquences très néfastes sur la stabilité de Gradlon, qui sombra petit à petit dans la paranoïa.

## La démesure de Gradlon

Ys Gradlon déclara que le projet devait passer entièrement sous contrôle de GUN. Si dans les faits toutes les sociétés qui travaillaient sur le projet pûrent continuer, l’ile se militarisa de plus en plus. Les habitants du peuple Sidh sur l’ile furent déplacé dans des dortoirs, afin de s’assurer de l’absence de traître. Il commença à s’intéresser également aux application militaire du projet. Il considérait l’attaque des Sidh comme une trahison envers la Fédération même, et commença a craindre plus d’ennemi intérieurs. Faisant des cauchemars chaque nuit, devenant de plus en plus méfiant, il garda le même but qu’avant – protéger les Fédérations Unis et ses habitants – mais devint prêt à le faire à tout prix.

Il fit étudier le Stygian Realm, et la manière dont la faille pouvait happer êtres vivants, songeant aux possibilités de l’utiliser pour faire disparaître des « ennemis des United Federation » (c’est-à-dire les forces militaires opposées à l’unification des états dans la Fédération, les grands bandits, et les groupes terroristes). Il voyait cette dimension comme « la prison ultime », puisque ce serait une prison ou il serait impossible de s’évader, à cause de la corruption provoqué par le Styx. Il commença à théoriser qu'en fin de compte, les mobiens n'étaient rien de plus que des aliens proche à envahir la Terre.

Ce fut à ce moment là que Dahut commença a se rebeller. Elle refusa de coopérer, estimant qu’une telle prison était contraire à ce que la Fédération Unie et leur projet tenaient. Ys Gradlon s’énerva, et fit emprisonner Dahut. Le Syphon d’Energie du Gosth Core fut maintenu par une plus petite équipe, dont une partie des membres était dévoué à garantir la stability du Stygian Rift.
Ys Gradlon fit construit une armée de satellite, les Erinyie, capable de capter l’énergie de la centrale et de se la transmettre, afin de créer une armada de satellite capable de toucher n’importe quel point de la terre. Les forces automatisé de construction furent lancé en orbite, et les Fédérations Unis seraient doté de la capacité de frapper partout sur la Terre, à n’importe quel moment.

Les Fédérations Unis seraient désormais protégée de toute menace.

## La fin du projet.

Cependant, Dahut fini par réussir à s’évader. Elle vit ce qu’était devenu le projet : en place de ville du futur, d’utopie pour l’avenir, il n’y aurait qu’une arme ultime. Elle décida alors de détruire toute l’œuvre de sa vie – ce pour quoi elle avait été même prête à trahir sa famille – et possiblement se tuer avec : Saboter la Gosth Central, pour mettre fin définitivement au projet.

Les humains la retrouvèrent en train de saboter plusieurs parties de la centrale, brisant tuyaux, détruisant machines de contrôles. Avait-elle pour but de faire exploser la centrale ? Elle fut attrapé après son œuvre, et exécutée après son refus de dire comment réparer ce qu’elle avait fait. Cependant, Gradlon refusa d’évacuer l’île, et ordonna de couper la centrale le temps de réparer les dégats.

Ce fut là que l’incident se produisit. Dahut avait réussi à placer au fond du Stygian Altar, dans une petite crypte qu’aucun humain ne connaissait, un appareil de sa création. Un petit appareil qui capitait l’énergie et la renvoyait, de sorte à faire saturer toute l’énergie Stygienne. Comme un immense court-circuit d’énergie, à l’intérieur même de la faille. Cela provoquerait le même phénomène de « happe » qui avait touché Bénélos et qu’utilisait Gradlon, mais à l’échelle de l’ile entière. Les sabotages n’avaient été qu’une couverture à sa véritable action, et la machine n’avait pas été retrouvée.

La saturation d’énergie se produisit donc, et, en une fraction de seconde, il ne resta plus un seul humain ou sidh sur l’île de Gosth Island. De nombreux dégats furent produits sur les mini-générateurs Chaotique de Sunlit Island également. Face à la disparition de leur commandant, et à la perte de toutes les personnes savant faire fonctionner l’île, elle fut désaffectée, et l’île passa sous surveillance de GUN pendant 15 ans.

Ce fut la fin du projet Millenium.
