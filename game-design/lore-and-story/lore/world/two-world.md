# Sonic Radiance - Les deux mondes

Sonic Radiance se base au niveau de son univers sur le concept des deux mondes (à la Sonic X) : Deux univers coexistent, et les personnages peuvent passer de l'un à l'autre. Ces deux mondes sont ceux de la Terre, une planète organisée et peuplée surtout par les humains, et un monde sans nom nommé le "monde des animaux", peuplé surtout par des animaux anthropomorphiques intelligents.

Le passage d'un monde à l'autre est conduit par l'existence de *Super Warp Ring*, des immenses anneaux qui ont le pouvoir d'être des passage entre les deux mondes. Ils étaient inactif depuis des milliers d'année quand les *expéditions interdimensionnelles* d'il y a 50 ans, puis *l'incident de Christmas Island* ont provoqué leur réactivation, et la connexion des deux mondes. Depuis, les deux mondes se connaissent, et doivent coéxister, non sans quelques… réserves, parfois.

## Les quatres grandes civilisations

Les quatres grandes civilisations sont les quatres peuples qui ont les premiers été capable de voyager entre les mondes et de construire quelque chose hors de leur monde d'origine. Ce sont tous des peuples originaires du monde des animaux.

### La première grande civilisation

Peu est connu de la première grande civilisation. Ils sont surnommés les *Premiers* et autres surnoms vagues du genre, mais n'ont laissé que peu de trace, à part quelques éléments.

Ils sont théorisés comme étant les premiers utilisateurs de la puissance des *Chaos Emerald*, des *Power Rings*, et comme étant ceux qui ont créé de nombreux artefacts et entités tels que les *Super Warp Rings*. Nous ne savons cependant rien de leur culture, ni de leur évolution. Ils semblent avoir été présent sur les deux planètes, et avoir prospéré il y a plus de 10 000 ans.

### La seconde grande civilisation

***TODO***: écrire un truc dessus.

### La troisième grande civilisation

(note : voir *peuple-sidh*)

La troisième grande civilisation est le peuple Sidh ancien, qui a proposéré sur Sunlit Island.

### La quatrième grande civilisation

***TODO***: Choisir si je reprend les Nocturnus ou en fait une civilisation qui aurait exclavagiser les échidnés ?
