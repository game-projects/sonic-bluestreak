# La Terre

La "Terre" de Sonic Radiance est une terre alternative à la notre, ou les pays sont principalement remplacé par leurs Erzats de Sonic Radiance, et où la forme des continent est légèrement différent.

Cette Terre possède comme principale puissance politique les *Fédérations Unis*, et comme plus grande force militaire les *Guardian Units of Nations* (ou GUN).

## Les Fédérations Unis

Les Fédérations Unis sont la république parlementaire avec un régime présidentiel qui couvre une grande partie de la planète Terre. Plus puissante nation de la planète, elle a subit plusieurs crise depuis le retour du Docteur *Ivo Robotnik* sur Terre lors de l'incident de Christmas Island.

Elles peuvent être vu comme quelque chose entres les Nations-Unis et les États-Unis. Le but de cet état est de garantir un développement tout en respectant les libertés des pays qui en font partie (et qui conserve une certaine indépendance). L'île de Sunlit Island est sous contrôle des Fédérations Unis, avec comme représentant le maire de la ville de *Sunlit Metropolis*.

Leur technologie est majoritairement contemporaire, mais a réussi de grande avancée depuis 50 ans à l'aide des technologies chaotique, en grande partie découvertes par le professeur Gérald

Dans le jeu, les Fédérations Unis, n'ont pas une présence très forte dans le jeu, en tout cas pas directement.

## Les Gardian Units of Nations

Les Gardian Units of Nations (ou G.U.N. parfois orthographié en GUN) sont la principale force militaire des Fédérations Unis, et ont comme rôle de protéger à la fois les Fédérations Unis, et à la fois les pays alliés. Cette armée est dirigé par un Commandant.

L'armée est actuellement en grande partie dédiée à affronter le Dr. Ivo Robotnik, qui vise à conquérir le monde, mais n’arrive pas à le localiser et à lancer des attaques précises. La capacité à disparaître de ce savant étant déconcertante, et ses moyens encore inconnus. L'inefficacité de GUN à vaincre les menances originaire du monde des animaux est la source d'une méfiance chez certaines personnes.

## Rimlight, les Chaos Central et les Chaos Vein

La société Rimlight est l’un des éléments scénaristique important du jeu. Holding co-détenu par des actionnaires privés et par le GUN, la société recherche dans le domaine des énergies propres, et plus précisément dans le domaine des énergies chaotiques.

Ils utilisent notamment pour cela les thèses de Gerald Robotnik sur ce type d’énergie, quand il était chercheur à l’université de Central City, thèses qui avaient conduit Gerald à faire des recherches sur les civilisations anciennes échidnés, pour mieux comprendre leur rapport à l’énergie chaotique (d’où les découvertes sur Chaos, la prophétie échidné, Gizoïd…) et le fait qu’il fut choisit comme chef de l’équipe du projet Shadow. Ils sont les producteurs des Chaos Drive, et des Chaos Central.

### Les Chaos Central

Les Chaos Central sont une source d’énergie de plus en plus présente sur toute la planète. Construite sur des zones nommées les Veines Chaotiques (Chaos Vein) et sont utilisées depuis environs vingt ans (pour le marché publique) à vingt-cinq ans (pour les Guardian Units of Nations). L’énergie produite par les Chaos Central revet deux forme : l’éléctricité, ou les Chaos Drives, des batteries chaotiques extrêmement durable utilisée notamment par les mécha des Guardian Units of Nations.

L’intérieur d’une centrale chaotique est cependant un endroit dangereux à cause de l’influence de l’énergie chaotique et de sa transformation en électricité ou en *Chaos Shard* pour les Chaos Drive, et est réservé à des connaisseurs de l’énergie chaotique. Le cœur d’une centrale chaotique, nommé le *Chaos Core*, est un endroit ou l’énergie circule suivant des patterns très précis.

C’est la deuxième source d’énergie la plus utilisée, après l’énergie thermique liquide produite par la société HexaECO, utilisée depuis à peu près la même période.

### Les Chaos Vein

Les Chaos Vein (Veines Chaotiques) sont la source de l’énergie utilisée dans les Chaos Central du conglomérat Rimlight. Il s’agit de zones d’émission spontanée d’Énergie Chaotique qui existent à quelques endroits de la planète, relativement rares mais trouvable sur tout les continents. Même si ces failles sont rien face à des Chaos Emerald, il est possible de produire de une énergie propre et durable à l’aide de ces failles. Quand elles ne sont pas sur-exploité (comme ce qui est arrivé dans un incident avec l’une des premières centrales chaotiques), ces failles sont contrairement au Chaos Emerald particulièrement docile à utiliser.

Les Chaos Vein sont des failles vers des univers-bulles qui leur sont propre (des sortes de Special Zone à petite échelle, mais qui ne contiennent pas de Chaos Emerald) et qui est la source de cette énergie. C’est ce qui fait l’aspect durable et stable à la fois de cette énergie : seul très peu d’énergie s’échappe de la veine, mais la reserve qu’il y a derrière est largement au dessus de toute la consommation humaine planétaire depuis les débuts de la révolution industrielle, au bordure de l’immesurable.
