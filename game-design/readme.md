# Sonic Bluestreak - game-design

## Types de jeux

Le but est de pouvoir proposer plusieurs types d'objectif
- Atteindre la fin du niveau + Atteindre fin niveau en un certain temps.
- Sauver n animaux.
- Récupérer n rings
- Récupérer n objects cachés (chasse au trésors)
- Terminer niveau avant autre perso
- Vaincre autres perso (deathmatch). Mode vie & victoire ?
- Vaincre boss
- "Special Stage"
- "Adventure Field" (pas d'objectif précis, mode "RPG")

## Types de niveaux

- Courses (Shadow Shoot)
- Arènes (Sonic Battle)
- Map Tiled
- Mode Tornado ? (Shooter 2D + classique... Peut-être le hardcoder comme un truc totalement différent)
- World Map ? (grande map avec vue de plus loin, où on pourrait booster librement à travers une map à la Adventure of Link, avec combat aléatoire, endroit caché, grand lieux, etc)

## "Effet de niveaux" ?

Des règles de niveaux qu'on pourrait mettre de manière supplémentaire sur une map ?
- Pas de saut possible
- Mode "pacifiste" (pas d'attaque possible)