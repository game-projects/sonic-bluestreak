# Sonic Radiance Credits

## GameDesign and Programming

- Kazhnuz

## Engine used

- [Love2D](https://love2d.org/)

## Visuals

- Background and Tiles from [Shadow Shoot](https://www.spriters-resource.com/mobile/shadowshoot/) ripped by Kazhnuz and Mr. C.

- HUD elements from [FieryExplosion](https://www.spriters-resource.com/submitter/FieryExplosion/)
